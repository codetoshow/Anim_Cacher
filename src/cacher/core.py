"""
Core module for cacher tool

:created Aug 23, 2015
:author: kenmohamed

"""
# STANDARD IMPORTS
import logging
from os import makedirs as os_makedirs
from os import path as os_path
from subprocess import PIPE as subprocess_PIPE
from tempfile import NamedTemporaryFile

# LOCAL IMPORTS
from src.job.job_framework import Job
from src.job.job_framework import JobQueue
from src.utils_.popen_wrapper import subprocess_popen
from src.utils_.files import copy_file
from src.etc import cacher_config

# logging setup
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

LOG.debug("LOADED: %s", __name__)


class CacherBusinessRules(object):
    """
    Business Rules for Cacher Tool
    (An "entity" in the BIE Model)
    """
    def __init__(self):

        super(CacherBusinessRules, self).__init__()

        self.rb_goods = cacher_config.BUSINESS_RULES_CONFIG["project_name"] # project name
        self.rb_cache_dir = cacher_config.BUSINESS_RULES_CONFIG["cache_directory"] # caches directory
        self.rb_feature = cacher_config.BUSINESS_RULES_CONFIG["artist_dept"] # artist department
        self.frame_padding = "4"

    def build_output_filename(self, ui_data):
        """
        Builds filename for output cache.
        Business Rules:
        BR1 : The filename tokens are limited to
              alphanumeric and underscore characters

        BR2 : The filename exists of period-separated tokens

        BR3 : The 3 filename tokens (+1 extension) are the following:
            project        --> os_getenv("RB_GOODS"), e.g. JUEJI_movie
            asset_type     --> from user-input, e.g. camera, prop...
            element_name   --> from Maya-scene-file, e.g. creature, cam_main1...
            type           --> hard-coded ("abc")

            E.g.  <Project>.<asset_type>.<element_name>.abc

        :param ui_data [dict]
        :return: output_filename [str]
        """
        asset_type = ui_data["asset_type"]["text"]
        element_name = ui_data["element_name"]

        LOG.debug(">>>>>>>>>> build_output_filename ARGS...")
        LOG.debug(">>>>>>>>>>\t\telement_name: %s", element_name)
        LOG.debug(">>>>>>>>>>\t\tasset_type: %s", asset_type)

        # BR1 ---------------------------------------------
        if element_name:
            element_name = self.get_valid_name(element_name)

        # BR2/3 ---------------------------------------------
        project = self.rb_goods
        output_filename = "{}.{}.{}.abc".format(project, asset_type, element_name)
        LOG.debug(">>>>>>>>>>    output_filename: %s", output_filename)

        return output_filename

    def build_output_path(self, _get_next_version, output_filename=None):
        """
        root_cache_path/fx/asset_type.element_name/version/abc
        :param output_filename
        :return: output_path
        """
        filename_parts = output_filename.split(".")
        asset_type = filename_parts[1]
        element_name = filename_parts[2]
        output_format = filename_parts[3]
        root_cache_path = self.rb_cache_dir
        department = self.rb_feature
        category_element = ".".join([asset_type, element_name])

        version = _get_next_version(out_filename=output_filename,
                                   asset_type=asset_type
                                  )
        version_path_component = "v" + str(version)

        return "%s/%s/%s/%s/%s" % (root_cache_path,
                                   department,
                                   category_element,
                                   version_path_component,
                                   output_format)

    @staticmethod
    def get_valid_name(a_string):
        """Replace non-alphanumeric characters with an underscore """
        result = "".join([a_char if a_char.isalnum() else "_" for a_char in a_string])
        return result

    @staticmethod
    def frange_expression_string(frange, step=1):
        """
        Builds a frame-expression given frange and step.
        Overloaded to accommodate database-Aspect.

        :param frange [tuple] (start, end)
        :param step [int or str]
        :return: [str] <start_frame>-<end_frame>:<step>
        """
        print "frange: ", frange
        # handle single frame
        if len(frange) == 1:
            frange = list(frange)
            frange.append(frange[0])
        fes = "{}-{}:{}".format(int(frange[0]), int(frange[1]), str(step))

        return fes



class CacherControl(object):
    """
    USE-CASE Controller
        single/multi asset local caching in background process

        future cases(remote/farm)
     ("Interacter" in the BIE-Model)
     ("Product", in the Builder pattern)
    """
    def __init__(self):
        super(CacherControl, self).__init__()
        # These are set in CacherGUIApplicationConfig (in assembler.py)
        # ("setter"-Dependency-Injection)
        self.gui = None
        self.maya_interface = None
        self.db_interface = None
        self.caching_business_rules = None

        # The following "interface" attrs are wrapped using class-methods (below) which
        # are called by the assembler.  Needing the wrapper-methods is a consequence of using
        # "setter" in place of "constructor" Dependency-Injection; if using
        # constructor-DI, the 4 class-attrs above would immediately have their values and
        # the following "interface"-attrs would be mapped to the interface-methods in
        # this constructor...

        # INTERFACE ATTRS
        # Maya
        #    signals
        self.selectionChanged = None
        self.timeChanged = None
        self.timeSliderChanged = None
        #    mayapy pathfile
        self.mayapy = None
        self.maya_standalone_setup = None
        #    methods
        self.time_slider_frange = None
        self.current_frame = None
        self.scene_frange = None
        self.scene_filename = None
        self.object_type = None
        self.transforms_in_set = None
        # Business Rules
        self.build_output_filename = None
        self.get_valid_name = None
        self.frange_expression_string = None
        self.build_output_path_ = None
        self.frame_padding = None
        self.rb_goods = None
        # Database
        self.cache_metadata = None
        self.save_cache_metadata = None
        self.query_dag_element_name = None
        self.query_entity_versions = None

        # "element_name" is initially derived from root-node-name/src_dag_path
        # Want to persist as {src_dag_path:element_name} in order to RECALL
        # the [generated|edited] element_name -- to save user from
        # re-entering custom element_name
        self.existing_element_names_dict = {}

    def execute(self):
        """Called from main.py"""
        self._launch_cacher_window()

    def wrap_maya_interface(self):
        """Called by assembler"""
        #    signals
        self.selectionChanged = self.maya_interface.selectionChanged
        self.timeChanged = self.maya_interface.timeChanged
        self.timeSliderChanged = self.maya_interface.timeSliderChanged
        #    mayapy pathfile
        self.mayapy = self.maya_interface.mayapy
        self.maya_standalone_setup = self.maya_interface.maya_standalone_setup
        #    methods
        self.time_slider_frange = self.maya_interface.time_slider_frange
        self.current_frame = self.maya_interface.current_frame
        self.scene_frange = self.maya_interface.scene_frange
        self.scene_filename = self.maya_interface.scene_filename
        self.object_type = self.maya_interface.object_type
        self.transforms_in_set = self.maya_interface.transforms_in_set

    def wrap_db_interface(self):
        """Called by assembler
        "cache_metadata"- dictionary from database table columns
       """
        self.cache_metadata = self.db_interface.cache_db_data_empty_dict
        self.save_cache_metadata = self.db_interface.register_cache
        self.query_dag_element_name = self.db_interface.query_dag_element_name
        self.query_entity_versions = self.db_interface.query_entity_versions

    def wrap_business_rules_interface(self):
        """Called by assembler

        Business Rules interface

        """
        self.build_output_filename = self.caching_business_rules.build_output_filename
        self.get_valid_name = self.caching_business_rules.get_valid_name
        self.frange_expression_string = self.caching_business_rules.frange_expression_string
        self.build_output_path_ = self.caching_business_rules.build_output_path
        self.frame_padding = self.caching_business_rules.frame_padding
        self.rb_goods = self.caching_business_rules.rb_goods

    def build_output_path(self, output_filename=None):
        """
        "_get_next_version" accesses db, so its defined in this class.
        "build_output_path" needs it, so pass it along from here
        """
        return self.build_output_path_(self._get_next_version, output_filename=output_filename)

    # --------------------------------------------------

    def _launch_cacher_window(self):
        self.gui.set_controller(self)
        self.gui.create_window()
        self.gui.show()


    def get_element_name(self, dag_path):
        """
        Look for element_name in data-structures, with "dag_path" as key:
            First,  in memory (existing_element_names_dict)
            Second, in db
        If still not found, then derive_element_name using business_rules
        update self.existing_element_names_dict
        :param: dag_path - of transform from Maya scene-file
        :return: element_name
        """
        # check memory
        if dag_path in self.existing_element_names_dict:
            element_name = self.existing_element_names_dict[dag_path]
            LOG.debug(">>>>>>>>>> element_name recalled from memory: %s", element_name)
        else:  # check shot db
            element_name = self.query_dag_element_name(dag_path=dag_path)
            LOG.debug(">>>>>>>>>> element_name recalled from ShotDB: %s", element_name)
        if not element_name: # otherwise, derive DEFAULT name from dag_path
            element_name = self.get_valid_name(dag_path)
            LOG.debug(">>>>>>>>>> element_name derived from Transform: %s", element_name)

        return element_name

    def _get_next_version(self, out_filename=None, asset_type=None):
        """
        Gets HIGHEST existing version and increments it to get the next_version

        :param out_filename, versionless filename
        :param asset_type
        :return: [str] next_version
        """
        highest_existing_version = self.query_entity_versions(out_filename, asset_type)
        next_version = str(int(highest_existing_version) + 1)

        LOG.debug(">>>>>>>>>>    next_version: %s", next_version)
        return next_version

    def _collect_cache_metadata(self, this_dag_data):
        """
        Database rules (i.e. from schema)

        cache_metadata["it_name"]
              a dot-connected token group
              i.e. "asset_type"."element_name"

        asset_category
            a slash-connected togen group
            e.g. "%s/%s" % (asset_category, "geometry")

        :param: this_dag_data - data for a specific dag-path
                "dag_data" is populated from cacher ui
        """
        self.cache_metadata["it_frame_paddings"] = self.frame_padding
        self.cache_metadata["it_formats"] = this_dag_data["cache_format"]
        self.cache_metadata["it_frame_range_expr"] = this_dag_data["frange"]
        self.cache_metadata["it_version"] = this_dag_data["version"]

        if this_dag_data["asset_type"]["text"] == "camera":
            asset_category = "%s/%s" % (this_dag_data["asset_type"]["text"], "camera")
        else:
            asset_category = "%s/%s" % (this_dag_data["asset_type"]["text"], "geometry")
        self.cache_metadata["it_category"] = asset_category

        self.cache_metadata["it_name"] = "%s.%s" % (this_dag_data["asset_type"]["text"], this_dag_data["element_name"])
        self.cache_metadata["it_output_name"] = "%s.%s.%s" % (self.rb_goods,
                                                              this_dag_data["asset_type"]["text"],
                                                              this_dag_data["element_name"])

    def create_cache(self, dag_data):
        """
        :param: dag_data - data for one or more cacheable-entities from Maya scene
                Populated from cacher ui
        """
        script_file = NamedTemporaryFile("wt")
        script_file.write(self.maya_standalone_setup)
        script_file.flush()

        queue = JobQueue()
        for dag_path in dag_data:
            # there is a ui-glitch that results in an entry with empty dag_path
            # added check
            if dag_path:
                this_dag_data = dag_data[dag_path]
                self._collect_cache_metadata(this_dag_data)
                this_dag_data["cache_metadata"] = self.cache_metadata

                queue.add_item(CacheJob(self, this_dag_data, script_file.name))

        #sub_queue = subprocess_popen("queue.run()")
        queue.run()

        LOG.info("DONE")



class CacheJob(Job):
    """

    """
    def __init__(self, cacher_control, dag_data, temp_maya_script_pathfile):
        super(CacheJob, self).__init__()
        LOG.debug("class CacheJob(Job).__init__(self)")

        self.mayapy = cacher_control.mayapy
        self.maya_scene = cacher_control.scene_filename()
        self.cacher_control = cacher_control
        self.cache_tempdir = os_path.dirname(temp_maya_script_pathfile)
        self.temp_maya_script_pathfile = temp_maya_script_pathfile
        self.dag_data = dag_data
        self.cache_attrs = {}
        self.cache_cmd = None

    def pre_job(self):
        """ Prepare for the main job.
        POPULATE cache_attrs from dag_data
        CREATE cache_cmd
        CREATE destination path
        CREATE cache_job_script
        """
        # POPULATE cache_attrs from dag_data (used to build cache cmd)
        transform_name = self.dag_data["dag_path"]
        self.cache_attrs["transform_name"] = transform_name
        temp_pathfile = os_path.join(self.cache_tempdir,
                                     self.dag_data["cache_filename"])
        self.cache_attrs["temp_pathfile"] = temp_pathfile

        frange = []
        frange_expression = self.dag_data["frange"]
        frange_frms, step = frange_expression.split(":")
        start_frame, end_frame = frange_frms.split("-")
        self.cache_attrs["start_frame"] = start_frame
        self.cache_attrs["end_frame"] = end_frame
        self.cache_attrs["step"] = step

        # CREATE cache_cmd
        asset_type = self.dag_data["asset_type"]["text"]
        #     camera cache command has arg"s that differ from mesh cache command
        if asset_type == "camera":
            self.cache_cmd = self.build_cam_ABC_cache_cmd()
        else:
            self.cache_cmd = self.build_geo_ABC_cache_cmd()

        # CREATE destination path


        self.try_makedirs(self.dag_data["cache_path"])

    def do_job(self):
        #DO the main job
        maya_scene = str(self.maya_scene)
        print "CACHE_JOB: {} from {}".format(self.dag_data["cache_filename"],
                                             maya_scene,
                                            )
        maya = subprocess_popen([self.mayapy,
                                 self.temp_maya_script_pathfile,
                                 self.cache_cmd,
                                 maya_scene,
                                ],
                                stdout=subprocess_PIPE,
                                stderr=subprocess_PIPE,
                               )
        out, err = maya.communicate()
        exitcode = maya.returncode
        if str(exitcode) != "0":
            print err
            print "error creating cache: {}".format(self.dag_data["cache_filename"])
        print out

    def post_job(self):
        """
        Move file from tempdir to final location
        Check for existence of cache-file
            if cache exists
                save cache-metadata
        """
        out_pathfile = os_path.join(self.dag_data["cache_path"],
                                    self.dag_data["cache_filename"],
                                   )

        copy_file(self.cache_attrs["temp_pathfile"], out_pathfile)

        if os_path.isfile(out_pathfile):
            print "{} EXISTS".format(out_pathfile)
            self.cacher_control.save_cache_metadata(self.dag_data["cache_metadata"])

    def build_cam_ABC_cache_cmd(self):
        """
        Builds a python-executable statement-string
        Overloaded to accommodate database-Aspect.

        :param abc_pathfile: cache export destination
        :param start_frame
        :param end_frame
        :returns: cam_cache_cmd, the python command to export the camera
        """
        export_cmd = ["pmc.AbcExport(verbose=True,"]
        job_parts = ['jobArg="',
                     "-frameRange",
                     self.cache_attrs["start_frame"],
                     self.cache_attrs["end_frame"],
                     "-step", self.cache_attrs["step"],
                     "-renderableOnly",
                     "-stripNamespace",
                     "-worldSpace",
                     "-eulerFilter",
                     "-root", self.cache_attrs["transform_name"],
                     "-file", self.cache_attrs["temp_pathfile"],
                     '")'
                    ]
        cam_cache_cmd_list = export_cmd + job_parts
        cam_cache_cmd = " ".join(cam_cache_cmd_list)
        LOG.debug(">>>>>>>>>>    cam_cache_cmd: %s", cam_cache_cmd)
        return cam_cache_cmd

    def build_geo_ABC_cache_cmd(self):
        """
        Builds a python-executable statement-string.

        :param out_pathfiles_dict = {<root_node_name_rig0>: <cache_out_pathfile0>,.
        :param start_frame
        :param end_frame
        :return: abc_cache_cmd
        """
        # BUILD AbcExport CMD Statement
        cache_cmd_list = ["pmc.AbcExport(verbose=True,"]

        job_args = ['jobArg="',
                    "-frameRange",
                    self.cache_attrs["start_frame"],
                    self.cache_attrs["end_frame"],
                    "-step", self.cache_attrs["step"],
                    "-renderableOnly",
                    "-stripNamespace",
                    "-uvWrite",
                    "-wholeFrameGeo",
                    "-writeVisibility",
                    "-worldSpace",
                    "-eulerFilter",
                    "-root", self.cache_attrs["transform_name"],
                    "-file", self.cache_attrs["temp_pathfile"],
                    '")'
                   ]

        cache_cmd_list += job_args

        mesh_cache_cmd = " ".join(cache_cmd_list)
        LOG.debug(">>>>>>>>>>    mesh_cache_cmd %s", mesh_cache_cmd)
        return mesh_cache_cmd

    def try_makedirs(self, path):
        """
        :summary: Wrapper for os_makedirs(path) for error handling
        :param path: full path to non-existing directory
        """
        dir_exists = os_path.isdir(path)
        if not dir_exists:
            try:
                os_makedirs(path)
            except Exception, msg:
                print "Problem creating {}\n{}".format(path, msg)

        dir_exists = os_path.isdir(path)
        return dir_exists
