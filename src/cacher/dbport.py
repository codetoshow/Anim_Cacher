"""
db port - location for all db access commands....
"""
# STANDARD IMPORTS
import logging

from datetime import datetime
from getpass import getuser
from uuid import uuid4

# LOCAL IMPORTS
from src.dbadapter.sqlite_adapter import SqliteAdapter as DB
# from dbadapter.sqlite_adapter import SqliteAdapter as DB
from src.etc import cacher_config


# logger setup
LOG = logging.getLogger(__name__)


LOG.setLevel(logging.INFO)
LOG.setLevel(logging.DEBUG)



class ShotDB(object):
    """Data container"""
    def __init__(self):
        LOG.debug(">>>>>>>>>>    ShotDB.__init__()")

        # Create handle to db
        self.dbb = DB(cacher_config.DB_ADAPTER_CONFIG["db_path"])

        # Create "cache_db_data_keys", a single list of columns to be
        # populated by cacher
        item_tags = self.dbb.item_tags_column_names
        item_details = self.dbb.item_detail_column_names
        cache_db_data_keys = item_details + item_tags

        self.cache_db_data_empty_dict = dict.fromkeys(cache_db_data_keys)
        LOG.debug("cache_db_data_empty_dict: %s", self.cache_db_data_empty_dict)

        self.item_tags = dict.fromkeys(item_tags)
        self.item_details = dict.fromkeys(item_details)

    def register_cache(self, cache_data):
        """rc"""
        it_id = str(uuid4())
        self.item_tags["it_id"] = it_id
        self.item_tags["it_created_group"] = cacher_config.DB_ADAPTER_CONFIG["artist_dept"]
        self.item_tags["it_created_user"] = getuser()
        self.item_tags["it_created_app"] = cacher_config.DB_ADAPTER_CONFIG["application_category"]
        self.item_tags["it_name"] = cache_data["it_name"]
        self.item_tags["it_category"] = cache_data["it_category"]
        self.item_tags["it_created"] = str(datetime.now())
        self.item_tags["it_pipe_status"] = "pending"
        self.item_tags["it_prod_status"] = "pending"

        self.item_details["it_id"] = it_id
        self.item_details["it_frame_paddings"] = cache_data["it_frame_paddings"]
        self.item_details["it_description"] = cache_data["it_description"]
        self.item_details["it_output_dir"] = cache_data["it_output_dir"]
        self.item_details["it_output_name"] = cache_data["it_output_name"]
        self.item_details["it_version"] = cache_data["it_version"]
        self.item_details["it_frame_range_expr"] = cache_data["it_frame_range_expr"]
        self.item_details["it_formats"] = cache_data["it_formats"]

        self.dbb.connect()
        self.dbb.add_item(item_tags_data=self.item_tags, item_detail_data=self.item_details)
        """
        items = self.dbb.get_items()
        details = self.dbb.get_details_from([items[0][0]])
        print items
        print details
        """
        self.dbb.disconnect(commit=True)

    def query_entity_versions(self, out_filename, asset_category):
        """returns HIGHEST existing version"""
        self.dbb.connect()
        items = self.dbb.get_items()

        """
        print "\nGet all item tags data:"
        for item in items:
            print item
        """

        asset_category = "%s/%s" % (asset_category, "geometry")
        version = 0

        for version in self.dbb.get_versions_from(tags_filters={"it_name":out_filename, "it_category":asset_category}):
            if not version:
                version = 0
            else:
                #print "VERSIONS AVAILABLE: %s" % version
                version = version.replace("v", "")

        #print "\nGet all item detail data:"
        #for detail in cdb_int.self.dbb.get_details_from([item[0] for item in items]):
            #print detail

        self.dbb.disconnect()
        return version

    def query_dag_element_name(self, dag_path=None):
        """
        Added columns to colsItemDetailsList:
        ["it_dag_path",       "text"]
        ["it_element_name",   "text"]
        """


if __name__ == "__main__":
    SDB = ShotDB()
    LOG.debug("db: %s", SDB)
