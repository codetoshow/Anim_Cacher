"""
Maya Interface for the Cacher Tool

:created: Sep 12, 2015
:author: kenmohamed
"""
# STANDARD
import logging

# THIRD PARTY
from maya.cmds import file as cmds_file
from maya.cmds import currentTime as cmds_currentTime
from maya.cmds import listConnections as cmds_listConnections
from maya.cmds import ls as cmds_ls
from maya.cmds import objectType as cmds_objectType
from maya.cmds import playbackOptions as cmds_playbackOptions
from maya.OpenMaya import MEventMessage as om_MEventMessage

import pymel.core as pmc

from PySide2.QtCore import QObject
from PySide2.QtCore import Signal
# from Finder.Finder_items import item

# logger
LOG = logging.getLogger(__name__)

#   S E T   L O G G I N G   L E V E L
LOG.setLevel(logging.INFO)
LOG.setLevel(logging.DEBUG)

LOG.debug("LOADED: %s", __name__)

SCRIPT = """import sys
import maya.standalone as standalone
cmd_to_run_in_maya = sys.argv[1]
maya_scenefile = sys.argv[2]
try:
    standalone.initialize(name="python")
    import maya.cmds as cmds
    import pymel.core as pmc
    cmds.loadPlugin("AbcExport")
    cmds.loadPlugin("gpuCache")
    cmds.file(maya_scenefile, open=True, force=True)
except:
    sys.stderr.write( "Failed in initialize standalone application" )
    raise
sys.stderr.write( "EXECUTING: %s" % cmd_to_run_in_maya)
exec(cmd_to_run_in_maya)
"""


class MayaInterface(QObject):
    """
    Maya-access methods
    Maya-Event Signal hooks
    """
    # signals triggered by Maya events
    # Triggers are setup in "setup_cacher_maya_hooks", below
    selectionChanged = Signal(list)
    timeChanged = Signal(list)
    timeSliderChanged = Signal(tuple)

    def __init__(self):
        super(MayaInterface, self).__init__()
        LOG.debug(">>>>>>>>>>    MayaInterface.__init__()")

        self.setup_cacher_maya_hooks()
        global SCRIPT
        self.maya_standalone_setup = SCRIPT
        #self.mayapy = "/Applications/Autodesk/maya2016/Maya.app/Contents/bin/mayapy"
        self.mayapy = "/Volumes/Local/Applications/Autodesk/maya2017/Maya.app/Contents/bin/mayapy"

    @staticmethod
    def current_frame():
        """
        cmds.currentTime returns a float.  Convert to string in tuple
        without decimal
        """
        current_time_float = cmds_currentTime(query=True)
        current_frame = (str(int(current_time_float)))
        return current_frame

    @staticmethod
    def scene_frange():
        """
        Queries Maya open-scene file for frame-range data.

        :return: frange [tuple] (start, end)
        """
        start_frame = cmds_playbackOptions(query=True, animationStartTime=True)
        end_frame = cmds_playbackOptions(query=True, animationEndTime=True)
        frange = (start_frame, end_frame)
        return frange

    @staticmethod
    def time_slider_frange():
        """t"""
        start_frame = cmds_playbackOptions(query=True, minTime=True)
        end_frame = cmds_playbackOptions(query=True, maxTime=True)
        frange = (start_frame, end_frame)
        return frange

    @staticmethod
    def scene_filename():
        """t"""
        return cmds_file(query=True, sceneName=True)

    @staticmethod
    def object_type(item):
        """t"""
        type_ = cmds_objectType(item)
        if type_ == "objectSet":
            return "set"
        return type_

    def transforms_in_set(self, maya_set):
        """t"""
        transforms = []
        set_members = cmds_listConnections(maya_set)
        if set_members:
            for item in set_members:
                if self.object_type(item) == "transform":
                    transforms.append(item)
        return transforms

    def setup_cacher_maya_hooks(self):
        """
        CONNECT Maya-Event Messages to maya_int Signals using callbacks.
        Only required when running with a Maya-UI session
        """
        def mecb_emit_selchanged(__):
            """MAYA-Event CallBack [mecb]"""
            print "IN mecb_emit_selchanged"
            selection = cmds_ls(selection=True)

            self.selectionChanged.emit(selection)
        #     connect callback to maya-event message "SelectionChanged"
        om_MEventMessage.addEventCallback("SelectionChanged", mecb_emit_selchanged)

        def mecb_emit_timechanged(__):
            """MAYA-Event CallBack [mecb]"""
            time = pmc.animation.getCurrentTime()
            self.timeChanged.emit(time)
        #     connect callback to maya-event message "timeChanged"
        om_MEventMessage.addEventCallback("timeChanged", mecb_emit_timechanged)

        def mecb_emit_timesliderchanged(__):
            """MAYA-Event CallBack [mecb]"""
            print "IN mecb_emit_timesliderchanged"
            frange = self.time_slider_frange()
            self.timeSliderChanged.emit(frange)
        #     connect callback to maya-event message "playbackRangeChanged"
        om_MEventMessage.addEventCallback("playbackRangeChanged", mecb_emit_timesliderchanged)
