"""Entry point for the anim cacher tool
"""
# STANDARD IMPORTS
import os
import sys

# LOCAL IMPORTS
from assembler import CacherGUIApplicationConfig
from assembler import UseCaseAssembler

ASSEMBLER = False



def launch():
    """ Cache-Tool Access Point """
    global ASSEMBLER

    if not ASSEMBLER:
        ASSEMBLER = True
        use_case_config = CacherGUIApplicationConfig()
        assembler = UseCaseAssembler(use_case_config)
        use_case_controller = assembler.get_use_case()
        use_case_controller.execute()





