#!/Applications/Autodesk/maya2016/Maya.app/Contents/bin mayapy
"""

"""

# STANDARD IMPORTS
from os import environ as os_environ
import sys

# THIRD PARTY IMPORTS
import maya.standalone

# LOCAL IMPORTS
from shipper.cacher import Cacher
from shipper.cacher_db_int import ShotDB
from shipper.cacher_maya_int import MayaInterface
from shipper.cacher_user_int import CacherUI

os_environ["MAYA_LOCATION"] = "/Applications/Autodesk/maya2016/Maya.app/Contents"
os_environ["PYTHONHOME"]    = "/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/Python"
os_environ["PATH"] = "/Applications/Autodesk/maya2016/Maya.app/Contents/bin:" + os_environ["PATH"]
"""
sys.path.append("C:\Program Files\Autodesk\Maya2011\Python\lib\site-packages\setuptools-0.6c9-py2.6.egg")
sys.path.append("C:\Program Files\Autodesk\Maya2011\Python\lib\site-packages\pymel-1.0.0-py2.6.egg")
sys.path.append("C:\Program Files\Autodesk\Maya2011\Python\lib\site-packages\ipython-0.10.1-py2.6.egg")
sys.path.append("C:\Program Files\Autodesk\Maya2011\Python\lib\site-packages\ply-3.3-py2.6.egg")                         
sys.path.append("C:\Program Files\Autodesk\Maya2011\\bin\python26.zip")
sys.path.append("C:\Program Files\Autodesk\Maya2011\Python\DLLs")
"""
sys.path.append("/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/Python")
sys.path.append("/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/lib/python2.7/site-packages")
sys.path.append("/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/bin")
sys.path.append("/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/lib")
sys.path.append("/Applications/Autodesk/maya2016/Maya.app/Contents/Frameworks/Python.framework/Versions/Current/lib/python2.7/site-packages/maya")

print sys.path
# THIS
import maya.standalone
maya.standalone.initialize(name="python")


"""
def test_build_geo_ABC_cache_cmd():
    # setup input to build_geo_ABC_cache_cmd
    cache_attrs = []
    frange = (1001, 1037)
    out_pathfile = os_path.join("just", "some", "path")
    print out_pathfile

test_build_geo_ABC_cache_cmd()
"""
# build_output_filename ---------------------------------------------
def test_build_output_filename_from_root_transform():
    """"""
    root_xform_name = "a_namespace:an_asset"
    cacher_instance = cacher.Cacher()
    output_filename = cacher_instance.build_output_filename(root_transform_name=root_xform_name)

    assert_equal(output_filename, "None.None.a_namespace_an_asset.abc")

def test_build_output_filename_from_custom_name():
    """"""
    edited_name = "Custom Element-Name"
    cacher_instance  = cacher.Cacher()
    output_filename = cacher_instance.build_output_filename(custom_name=edited_name)

    assert_equal(output_filename, "None.None.Custom_Element_Name.abc")

def test_build_output_filename_from_asset_category():
    """"""
    category= "prop"
    cacher_instance = cacher.Cacher()
    output_filename = cacher_instance.build_output_filename(asset_category=category)

    assert_equal(output_filename, "None.prop.None.abc")

 # build_output_pathfile  ---------------------------------------------

def test_build_output_path():
    cacher_instance  = cacher.Cacher()
    path = cacher_instance.build_output_path()

    assert_equal(path, "")

