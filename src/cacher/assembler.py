"""
Uses Constructor-Dependency-Injection to realize I/O-components
as plug-ins to core-components (interactors/use-case-objects)
"""
# STANDARD IMPORTS
import logging

# LOCAL IMPORTS
from src.utils_.pySide_utils import maya_main_window
from core import CacherControl
from core import CacherBusinessRules
from mayaport import MayaInterface
from dbport import ShotDB
from src.uiadapter.pyside_adapter import CacherUI


# logging setup
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


LOG.debug("LOADED: %s", __name__)


class UseCaseAssembler(object):
    """
    Creates and instance of the Use-Case-Controller (using self._builder)
    Performs the setter-dependency-injection for the UC-Controller

    (This is the "Director/Controller"-object of the Builder Pattern)

    """
    def __init__(self, builder):
        self._builder = builder

    def _assemble_use_case(self):
        """
        Performs the use-case assembly using a use-case-specific builder
        """
        self._builder.create_use_case()
        self._builder.set_gui()
        self._builder.set_maya_interface()
        self._builder.set_db_interface()
        self._builder.set_business_rules_interface()

    def get_use_case(self):
        """Assembles and returns the UC-Controller"""
        self._assemble_use_case()
        return self._builder.use_case


class UseCaseBuilder(object):
    """ Builder
    Provides the interface for building the Object
    """

    def __init__(self):
        self.use_case = None

    def create_use_case(self):
        self.use_case = CacherControl()



class CacherGUIApplicationConfig(UseCaseBuilder):
    """Concrete Builder
    Each UC-Controller gets its own Concrete Builder
    Configuration Object
    """
    def __init__(self):
        super(CacherGUIApplicationConfig, self).__init__()

    def set_gui(self):
        self.use_case.gui = CacherUI(parent=maya_main_window())

    def set_maya_interface(self):
        self.use_case.maya_interface = MayaInterface()
        self.use_case.wrap_maya_interface()

    def set_db_interface(self):
        self.use_case.db_interface = ShotDB()
        self.use_case.wrap_db_interface()

    def set_business_rules_interface(self):
        self.use_case.caching_business_rules = CacherBusinessRules()
        self.use_case.wrap_business_rules_interface()

