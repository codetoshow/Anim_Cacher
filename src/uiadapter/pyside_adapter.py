"""
:created: Aug 24, 2015
:author: kenmohamed

180805 - updated for Maya2017 - PySide2 in place of PySide
"""
# - for debug
import inspect
# STANDARD
import logging

# THIRD PARTY
from PySide2.QtCore import Qt
from PySide2.QtCore import Signal
from PySide2.QtGui import QPalette
from PySide2.QtWidgets import QAbstractItemView
from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QComboBox
from PySide2.QtWidgets import QDialog
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QLineEdit
from PySide2.QtWidgets import QListWidget
from PySide2.QtWidgets import QListWidgetItem
from PySide2.QtWidgets import QMainWindow
from PySide2.QtWidgets import QPlainTextEdit
from PySide2.QtWidgets import QPushButton
from PySide2.QtWidgets import QStatusBar
from PySide2.QtWidgets import QVBoxLayout
from PySide2.QtWidgets import QWidget

# logging setup
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

LOG.debug("LOADED: %s", __name__)
debug = LOG.isEnabledFor(logging.DEBUG)



class CacherUI(QMainWindow):
    """
    Implements PySide2 User Interface for the Anim-Cacher Tool

    :param parent Maya Parent Window;
    """
    create_cache_clicked = Signal(dict)

    def __init__(self, parent=None):
        LOG.debug(">>>>>>>>>>     CacherUI.__init__")
        # Window Settings
        self.WINDOW_VERSION = "0.4.0"
        self.WINDOW_TITLE = "Cacher  "
        WINDOW_FLAGS = Qt.Window | \
                       Qt.WindowStaysOnTopHint | \
                       Qt.CustomizeWindowHint | \
                       Qt.WindowCloseButtonHint | \
                       Qt.WindowTitleHint

        try:
            super(CacherUI, self).__init__(parent, WINDOW_FLAGS)
        except TypeError as e:
            LOG.error("Failure initialising Retrieval --> self: %r", e)
            raise

        self.setMinimumWidth(500)
        self.setMinimumHeight(300)
        # To free memory on exit
        self.setAttribute(Qt.WA_DeleteOnClose)

        self.controller = None

        # CLASS DATA ATTRS
        # Data structure to collect data from ui to be passed to UseCase
        self.ui_data = {"prompt": "Select Root Transform",
                        "dag_path": "",
                        "asset_type": {"index": 0, "text": ""},
                        "element_name": "",
                        "cache_filename": "",
                        # "cache_path":"",
                        "note": "",
                        "frange_type": {"index": 0, "text": "Shot Frame Range"},
                        "frange": "",
                        "current_frame": "",
                        "current_list_item": "",
                        # "version":""
                        }

        # data for caching one or more Maya entities - keyed on the entity's dag_path
        self.dag_data = {}

        # To hold selections before adding to list
        self.selections = []
        # structure to map "Cache-Out List" data to dag_data - {list_item:dag_path}
        self.list_item_data = {}

        self.list_item_dag_path = ""

        # This is a control structure for button ENABLE-STATE
        # ...UI State
        self.ui_data_status = {"valid_selected_transform": False,
                               "valid_asset_type": False,
                               "valid_element_name": False,
                               "valid_cache_out_list": False,
                               "valid_list_selection": False,
                               "valid_element_note": False,
                               "valid_frange_type": False,
                               "valid_element_frange": False,
                               "custom_frange_type": False,
                               }

        # "connect" to Note Editor Widget
        self.notes_editor = NoteEditor(parent=self)
        self.notes_editor.setModal(True)

    def set_controller(self, controller=None):
        """Dependency hook-up"""
        self.controller = controller
        LOG.debug("core: %s" % self.controller)

    def create_window(self):
        """Create, Setup, Layout and Connect widgets"""
        self.setWindowTitle("{0} {1}".format(self.WINDOW_TITLE, str(self.WINDOW_VERSION)))

        def create_widgets():
            """"""
            self.container = QWidget(self)

            first_prompt = "Please select a Transform or a Set"
            self.lbl_prompt = QLabel(first_prompt, parent=self.container)
            self.lbl_selectedNode = QLabel("Selected Transform",
                                           parent=self.container)
            self.LnEdt_selectedNode = QLineEdit(parent=self.container)
            self.lbl_assetCategoryTag = QLabel("Asset Type",
                                               parent=self.container)
            self.cmbBx_assetType = QComboBox(parent=self.container)
            self.lbl_elementName = QLabel("ELEMENT Name",
                                          parent=self.container)
            self.LnEdt_elementName = QLineEdit(parent=self.container)
            self.lbl_cacheFilename = QLabel("", parent=self.container)
            btn_txt = "Add ELEMENT to 'Cache-Out List'"
            self.btn_addCacheToList = QPushButton(btn_txt,
                                                  parent=self.container)
            self.lbl_cacheOutList = QLabel("Cache-Out List",
                                           parent=self.container)
            self.lstWdgt_list = QListWidget(parent=self.container)
            self.lbl_note = QLabel("Notes", parent=self.container)
            self.plnTxtEdt_note = QPlainTextEdit(parent=self.container)
            # btn_txt = "Edit Notes"
            # self.btn_editNote = QPushButton(btn_txt, parent=self.container)
            self.lbl_elementFRange = QLabel("Frames", parent=self.container)
            self.cmbBx_frangeType = QComboBox(parent=self.container)
            self.LnEdt_elementFRange = QLineEdit(parent=self.container)
            self.btn_cacheOut = QPushButton("Cache Out", parent=self.container)
            self.btn_cancel = QPushButton("Cancel", parent=self.container)
            self.statusbar = QStatusBar()

        def setup_widgets():
            """"SETUP widgets"""
            # Color Setup - Palette definition
            palette_prmpt = QPalette()
            palette = QPalette()
            palette_prmpt.setColor(palette_prmpt.Text, Qt.green)
            palette.setColor(palette.Text, Qt.red)

            # Color Setup - Palette assignment
            self.lbl_prompt.setPalette(palette_prmpt)
            self.LnEdt_selectedNode.setPalette(palette)
            self.LnEdt_elementName.setPalette(palette)

            # Alignment Setup
            self.lbl_selectedNode.setAlignment(Qt.AlignCenter)
            self.lbl_assetCategoryTag.setAlignment(Qt.AlignCenter)
            self.lbl_elementName.setAlignment(Qt.AlignCenter)
            self.lbl_elementFRange.setAlignment(Qt.AlignCenter)

            # Populate Combo-Boxes
            # frame_range_types = ["Shot Frame Range","Current Frame","Time Slider","Custom"]
            frame_range_types = ["Shot Frame Range", "Current Frame", "Time Slider"]
            asset_types = ["select type", "char", "prop", "set", "camera"]
            self.cmbBx_assetType.addItems(asset_types)
            self.cmbBx_frangeType.addItems(frame_range_types)

            # Set Widget"s Initial STATE
            self.LnEdt_selectedNode.setEnabled(False)
            self.cmbBx_assetType.setEnabled(False)
            self.LnEdt_elementName.setEnabled(False)
            self.btn_addCacheToList.setEnabled(False)
            self.plnTxtEdt_note.setReadOnly(True)
            self.cmbBx_frangeType.setEnabled(False)
            self.LnEdt_elementFRange.setEnabled(False)
            self.btn_cacheOut.setEnabled(False)
            self.lbl_cacheFilename.clear()

            # SET Selection Mode
            #   selectionMode, setSelectionMode
            self.lstWdgt_list.setSortingEnabled(True)
            self.lstWdgt_list.setSelectionMode(QAbstractItemView.SingleSelection)

        def layout_widgets():
            """ Create and populate Layout-widgets"""
            #     create layout widgets
            lyt_main = QVBoxLayout(self.container)

            lyt_selection = QVBoxLayout(self.container)
            lyt_tag = QVBoxLayout(self.container)
            lyt_element = QVBoxLayout(self.container)
            lyt_element_data = QHBoxLayout(self.container)
            lyt_frange_data = QHBoxLayout(self.container)

            lyt_topSetup = QVBoxLayout(self.container)

            lyt_cacheList = QVBoxLayout(self.container)

            lyt_note_control = QHBoxLayout(self.container)
            lyt_note = QVBoxLayout(self.container)

            lyt_buttons = QHBoxLayout(self.container)

            lyt_cacheDestination = QVBoxLayout(self.container)

            lyt_assetCategory = QVBoxLayout(self.container)
            lyt_assetData = QHBoxLayout(self.container)

            #    add widgets to layouts
            lyt_selection.addWidget(self.lbl_selectedNode)
            lyt_selection.addWidget(self.LnEdt_selectedNode)

            lyt_tag.addWidget(self.lbl_assetCategoryTag)
            lyt_tag.addWidget(self.cmbBx_assetType)

            lyt_element.addWidget(self.lbl_elementName)
            lyt_element.addWidget(self.LnEdt_elementName)

            lyt_element_data.addLayout(lyt_selection)
            lyt_element_data.addLayout(lyt_tag)
            lyt_element_data.addLayout(lyt_element)

            lyt_frange_data.addWidget(self.lbl_elementFRange)
            lyt_frange_data.addWidget(self.cmbBx_frangeType)
            lyt_frange_data.addWidget(self.LnEdt_elementFRange)

            lyt_topSetup.addLayout(lyt_element_data)
            lyt_topSetup.addLayout(lyt_frange_data)

            lyt_topSetup.addWidget(self.lbl_cacheFilename)
            lyt_topSetup.addWidget(self.btn_addCacheToList)

            lyt_cacheList.addWidget(self.lbl_cacheOutList)
            lyt_cacheList.addWidget(self.lstWdgt_list)

            lyt_note_control.addWidget(self.lbl_note)
            # lyt_note_control.addWidget(self.btn_editNote)
            lyt_note.addLayout(lyt_note_control)
            lyt_note.addWidget(self.plnTxtEdt_note)

            lyt_buttons.addWidget(self.btn_cancel)
            lyt_buttons.addWidget(self.btn_cacheOut)

            lyt_main.addWidget(self.lbl_prompt)
            lyt_main.addSpacing(10)
            lyt_main.addLayout(lyt_topSetup)
            lyt_main.addLayout(lyt_cacheList)
            lyt_main.addLayout(lyt_note)
            lyt_main.addLayout(lyt_buttons)

            self.setCentralWidget(self.container)
            # self.setStatusBar(self.statusbar)

        def connect_signals():
            """"""
            self.cmbBx_assetType.activated.connect(lambda: self.cb_new_cacher_ui_input("asset_type"))
            self.LnEdt_elementName.returnPressed.connect(lambda: self.cb_new_cacher_ui_input("element_name"))

            self.btn_addCacheToList.clicked.connect(self.cb_force_note_creation)

            self.lstWdgt_list.currentItemChanged.connect(self.cb_new_list_selection)

            self.notes_editor.submit_note.connect(self.cb_new_note)

            self.cmbBx_frangeType.currentIndexChanged.connect(lambda: self.cb_new_cacher_ui_input("frange_type"))
            self.LnEdt_elementFRange.textEdited.connect(lambda: self.cb_new_cacher_ui_input("frange"))

            self.btn_cancel.clicked.connect(self.cb_cancel)
            self.btn_cacheOut.clicked.connect(self.cb_create_cache)

            # seems a bit "round-about".  Theres a port to the GUI (this module) and a port to Maya
            # both ports connect to the controller.  Here, the Maya is signalling (via the maya_port)
            # to the gui-port (this module) via the controller....
            if self.controller:
                self.controller.selectionChanged.connect(self.cb_new_maya_selection)
                self.controller.timeChanged.connect(self.cb_new_maya_time)
                self.controller.timeSliderChanged.connect(self.cb_new_maya_time_slider)

                self.create_cache_clicked.connect(self.controller.create_cache)

        create_widgets()
        setup_widgets()
        layout_widgets()
        connect_signals()
        return self

    # CallBack (cb_) methods  (triggered by signals)
    def cb_new_maya_selection(self, new_selection):
        """
        Callback for Signal from MAYA
        UI-Facing callback (affects ui)

        The "selection" is a selection in the MAYA ui (not cacher-tool-ui)
        >> this is triggered by a Maya-Event, NOT a cacher_ui-event

        Get element name based on selection"s dag_path
        Update ui

        When a transform is added to the Cache-Out list, its also added
        to self.dag_data.  This method checks self.dag_data to prevent
        adding a transform multiple times.

        GET Source Dag Paths
        GET Selection Data - dag-paths and element-names
        SET UI Data and Widget Status
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\ncb_new_maya_selection CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        # GET Source Dag Paths
        src_dag_paths = []
        for item in new_selection:
            item_type = self.controller.object_type(item)
            print "item_type", item_type
            if item_type == "set":
                # get the dag_paths for the transforms in the set
                src_dag_paths.extend(self.controller.transforms_in_set(item))
            elif item_type == "transform":
                src_dag_paths.append(item)

        # GET Selection Data - dag-paths and element-names
        self.selections = []  # reset selection data
        for src_dag_path in src_dag_paths:
            # filter out dags already captured
            if src_dag_path not in self.dag_data.keys():
                self.selections.append(src_dag_path)

        LOG.debug("self.selections: %s" % self.selections)

        # SET UI-Data, Widget Status (widgets for prompt, dag_path & element_name)
        if len(self.selections) == 0:
            # can be 0 if every selected dag has already been added to "Cache-Out List"
            dag_path = ""
            element_name = ""
            prompt = "Asset is already in 'Cache-Out List'"
        elif len(self.selections) == 1:
            dag_path = self.selections[0]
            element_name = self.controller.get_element_name(dag_path)
            prompt = "Select Asset Type"
        else:
            dag_path = "MULTIPLE_TRANSFORMS"
            element_name = "MULTIPLE_ELEMENTS"
            prompt = "Select Asset Type for Multiple Elements"

        # Set UI data
        self.ui_data["prompt"] = prompt
        self.ui_data["cache_filename"] = ""
        self.ui_data["element_name"] = "%s" % element_name
        self.ui_data["dag_path"] = "%s" % dag_path
        self.ui_data["asset_type"]["index"] = 0
        #    frange
        frange_type_index = self.cmbBx_frangeType.currentIndex()
        frange_type_text = self.cmbBx_frangeType.currentText()
        raw_frange = self.controller.scene_frange()
        frange = self.controller.frange_expression_string(raw_frange)

        self.ui_data["frange_type"]["index"] = frange_type_index
        self.ui_data["frange_type"]["text"] = frange_type_text
        self.ui_data["frange"] = frange

        self.ui_data_status["valid_selected_transform"] = True
        self.ui_data_status["valid_element_name"] = True
        self.ui_data_status["valid_list_selection"] = False

        """
        current_list_item = self.lstWdgt_list.currentItem()
        current_list_item_widget = self.lstWdgt_list.itemWidget(current_list_item)
        current_list_item_widget.setSelected(False)
        """
        self.update_ui()

    def cb_new_maya_time(self, time_float):
        """Callback for Signal from MAYA"""
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\ncb_new_maya_time CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        new_current_time = int(time_float)
        self.ui_data["current_frame"] = str(new_current_time)

        self.update_ui()

        if self.update_dag_data_enable():
            self.update_dag_data()

    def cb_new_maya_time_slider(self, frange):
        """Callback for Signal from MAYA"""
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\ncb_new_maya_time_slider CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        if self.cmbBx_frangeType.currentText() == "Time Slider":

            new_min_time = int(frange[0])
            new_max_time = int(frange[1])

            new_frange = self.controller.frange_expression_string((new_min_time, new_max_time))
            self.ui_data["frange"] = str(new_frange)

            self.update_ui()

            if self.update_dag_data_enable():
                self.update_dag_data()

    def cb_new_cacher_ui_input(self, ui_src_type):
        """
        self.ui_data.keys = ["prompt",         <<-- only TO ui
                            "dag_path",        <<-- only TO ui
                            "asset_type",      >>
                            "element_name",    >>
                            "cache_filename",  <<-- only TO ui
                            "note",            >>
                            "frange_type",     >>
                            "frange",          >>
                            "current_frame"    >>
                            "cache_list",      <<-- only TO ui
                            ]
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\nccb_new_cacher_ui_input CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        LOG.debug("NEW INPUT FROM UI ui_src_type: %s" % ui_src_type)

        # COLLECT NEW UI DATA into "ui_data"
        if ui_src_type == "asset_type":
            asset_type_index = self.cmbBx_assetType.currentIndex()
            asset_type_text = self.cmbBx_assetType.currentText()
            self.ui_data["prompt"] = ""
            self.ui_data["asset_type"]["index"] = asset_type_index
            self.ui_data["asset_type"]["text"] = asset_type_text

        if ui_src_type == "element_name":
            element_name = self.LnEdt_elementName.text()
            element_name = self.controller.get_valid_name(element_name)
            self.ui_data["element_name"] = element_name

        if ui_src_type == "asset_type" or "element_name":
            if self.ui_data["element_name"] == "MULTIPLE_ELEMENTS":
                cache_filename = "MULTIPLE_FILES"
            else:
                cache_filename = self.controller.build_output_filename(self.ui_data)
            self.ui_data["cache_filename"] = cache_filename

        if ui_src_type == "note":
            self.ui_data["note"] = self.plnTxtEdt_note.toPlainText()

        if ui_src_type == "frange_type":
            this_frange = ""
            current_frame = ""
            frange_type_text = self.cmbBx_frangeType.currentText()
            frange_type_index = self.cmbBx_frangeType.currentIndex()
            print "frange_type_text", frange_type_text
            if frange_type_text == "Shot Frame Range":
                raw_frange = self.controller.scene_frange()
                this_frange = self.controller.frange_expression_string(raw_frange)
            elif frange_type_text == "Current Frame":
                current_frame = self.controller.current_frame()
                this_frange = self.controller.frange_expression_string(current_frame)
            elif frange_type_text == "Time Slider":
                raw_frange = self.controller.time_slider_frange()
                this_frange = self.controller.frange_expression_string(raw_frange)

            self.ui_data["frange_type"]["index"] = frange_type_index
            self.ui_data["frange_type"]["text"] = frange_type_text
            self.ui_data["frange"] = this_frange
            self.ui_data["current_frame"] = current_frame

        # RESET WIDGET STATE
        self.ui_data_status["valid_element_name"] = bool(self.ui_data["element_name"])
        self.ui_data_status["valid_asset_type"] = bool(self.ui_data["asset_type"]["index"])
        self.ui_data_status["valid_element_note"] = bool(self.ui_data["note"])

        # Update dag data and ui
        if self.update_dag_data_enable():
            self.update_dag_data()

        self.update_ui()

    def cb_force_note_creation(self):
        """FORCE NOTE CREATION (A Business Rule)"""
        self.notes_editor.show()

    def cb_new_list_selection(self, current):
        """
        When the current QListWidget item changes,
        the QListWidget.currentItemChanged() signal is emitted with
        the new and previous items.

        get QListWidgetItem
        use it to get the dag_path from dag_data
        set ui_data from dag_data
        update_ui
        """
        LOG.debug("\n\n>>>>>>>>>> NEW NEW LIST SELECTION")

        this_pathfile = current.text()

        try:
            self.list_item_dag_path = self.list_item_data[this_pathfile]
            self.dag_data_to_ui_data(self.list_item_dag_path)
            self.ui_data["dag_path"] = ""
            self.ui_data_status["valid_selected_transform"] = False
            self.ui_data_status["valid_list_selection"] = True
            self.ui_data_status["valid_asset_type"] = True
            self.ui_data_status["valid_element_name"] = True

            self.update_ui()
        except KeyError:
            # When item is added to list, this method gets called, before
            # the item is added to self.list_item_data
            pass

    def cb_new_note(self, text):
        """"""
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\ncb_new_note CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("list_item_data%s" % self.list_item_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        if text:
            self.ui_data["note"] = text
            self.update_ui()

            for dag_path in self.selections:
                self.create_dag_data(dag_path)

            self.add_element_to_list()

            # update ui data and status
            self.ui_data["note"] = text
            self.ui_data["prompt"] = ""
            self.ui_data["dag_path"] = ""
            self.ui_data["asset_type"]["index"] = 0
            self.ui_data["element_name"] = ""
            self.ui_data_status["valid_element_note"] = True

            self.update_ui()

    def cb_cancel(self):
        """UI-Facing callback"""
        self.close()

    def cb_create_cache(self):
        """
        CORE-Facing signal-based callback
        """
        LOG.debug(">>>>>>>>>>    START CACHING")
        LOG.debug("DAG data: %s", self.dag_data)
        self.create_cache_clicked.emit(self.dag_data)
        self.close()

    # ------------------------------------
    def add_element_to_list(self):
        """
        Create cache_pathfile
        Create QListWidgetItem with cache_pathfile
        Add QListWidgetItem to self.lstWdgt_list

        Consider the following connections
            self.btn_addCacheToList.clicked.connect(self.cb_force_note_creation)
            self.notes_editor.submit_note.connect(self.cb_new_note)
        When addCacheToList fires, the Note Editor is launched
        Note Editor fires submit_note signal, calling cb_new_note
        cb_new_note CALLS THIS METHOD

        QListWidget NOTES:
            self.lstWdgt_list.sortItems([order=Qt.AscendingOrder])
            self.ui_data["cache_list"].append(cache_pathfile)

            To remove items from the list, use takeItem()
            The current item in the list can be found with currentItem(),
            and changed with setCurrentItem()
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\nadd_element_to_list CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("list_item_data:%s" % self.list_item_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        for dag_path in self.selections:
            this_dag_data = self.dag_data[dag_path]

            # Create new list-item
            path = this_dag_data["cache_path"]
            filename = this_dag_data["cache_filename"]
            pathfile = "%s/%s" % (path, filename)

            new_item = QListWidgetItem()
            new_item.setText(pathfile)

            # Add item to list
            self.lstWdgt_list.addItem(new_item)
            self.lstWdgt_list.setCurrentItem(new_item)

            # Add item to list_item_data
            self.list_item_data[pathfile] = this_dag_data["dag_path"]

        # UPDATE UI Status
        # self.ui_data_status["valid_list_selection"] = True
        self.ui_data_status["valid_cache_out_list"] = True
        self.ui_data_status["valid_selected_transform"] = False
        self.ui_data_status["valid_asset_type"] = False
        self.ui_data_status["valid_element_name"] = False
        self.update_ui()

    def delete_element_from_list(self, list_widget_item=None):
        """
        To remove items from the list, use PySide2.QtWidgets.QListWidget.takeItem()
        """
        pass

    def create_dag_data(self, dag_path):
        """"""
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\ncreate_dag_data CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        self.ui_to_dag_data(dag_path)

    def update_dag_data(self):
        """
        self.list_item_data = {}
            {list_item:dag_path}
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\nupdate_dag_data CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("list_item_data:%s" % self.list_item_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        self.ui_to_dag_data(self.list_item_dag_path)

    def ui_to_dag_data(self, dag_path):
        """Creates the dict "this_dag_data", including a subset of ui_data"
        collects results in "dag_data"; e.g.:

            self.dag_data[dag_path] = this_dat_data
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\nupdate_ui CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("list_item_data:%s" % self.list_item_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        asset_type_index = self.cmbBx_assetType.currentIndex()
        asset_type_text = self.cmbBx_assetType.currentText()
        note = self.plnTxtEdt_note.toPlainText()
        this_dag_data = dict()
        this_dag_data["cache_format"] = "abc"
        this_dag_data["dag_path"] = dag_path

        this_dag_data["asset_type"] = {}
        this_dag_data["frange_type"] = {}

        this_dag_data["asset_type"]["index"] = asset_type_index
        this_dag_data["asset_type"]["text"] = asset_type_text
        this_dag_data["note"] = note
        # get element_name, filename, path, version
        this_dag_data["element_name"] = self.controller.get_element_name(dag_path)
        c_fn = self.controller.build_output_filename(this_dag_data)
        c_p = self.controller.build_output_path(c_fn)
        version = c_p.split("/")[-2].split("v")[1]
        this_dag_data["cache_filename"] = c_fn
        this_dag_data["cache_path"] = c_p
        this_dag_data["version"] = version
        # get frange data
        this_dag_data["frange_type"]["index"] = self.ui_data["frange_type"]["index"]
        this_dag_data["frange_type"]["text"] = self.ui_data["frange_type"]["text"]
        this_dag_data["frange"] = self.ui_data["frange"]
        this_dag_data["current_frame"] = self.ui_data["current_frame"]

        self.dag_data[dag_path] = this_dag_data

    def dag_data_to_ui_data(self, src_dag_path):
        """self.ui_data["dag_path"] = src_dag_path"""
        this_dag_data = self.dag_data[src_dag_path]

        self.ui_data["asset_type"]["text"] = this_dag_data["asset_type"]["text"]
        self.ui_data["asset_type"]["index"] = this_dag_data["asset_type"]["index"]
        self.ui_data["element_name"] = this_dag_data["element_name"]
        self.ui_data["cache_filename"] = this_dag_data["cache_filename"]
        self.ui_data["cache_path"] = this_dag_data["cache_path"]
        self.ui_data["version"] = this_dag_data["version"]
        self.ui_data["note"] = this_dag_data["note"]
        self.ui_data["frange_type"]["index"] = this_dag_data["frange_type"]["index"]
        self.ui_data["frange"] = this_dag_data["frange"]
        self.ui_data["current_frame"] = this_dag_data["current_frame"]

        LOG.debug("self.ui_data" % self.ui_data)

    def update_ui(self):
        """
        Send ui_data to the ui
        updates UI-Elements with the now-current ui-state (persisted in self.ui_data)
        update predicates for ui state
            self.ui_data["prompt"] = "Please select ONE root transform"
            self.ui_data["dag_path"] = ""
            self.ui_data["asset_type"] = None
            self.ui_data["element_name"] = ""
            self.ui_data["note"] = note
            self.ui_data["frange_type"] = frange_type
            self.ui_data["frange"] = frange
        """
        if debug:
            try:
                current_frame = inspect.currentframe()
                call_frame = inspect.getouterframes(current_frame, 2)
                LOG.debug("\n\nupdate_ui CALLED BY:%s" % call_frame[1][3])
                LOG.debug("ui_data:%s" % self.ui_data)
                LOG.debug("dag_data:%s" % self.dag_data)
                LOG.debug("list_item_data:%s" % self.list_item_data)
                LOG.debug("selection_data:%s" % self.selections)
            finally:
                del current_frame
                del call_frame

        self.lbl_prompt.setText(self.ui_data["prompt"])
        self.LnEdt_selectedNode.setText(self.ui_data["dag_path"])
        self.cmbBx_assetType.setCurrentIndex(self.ui_data["asset_type"]["index"])
        self.LnEdt_elementName.setText(self.ui_data["element_name"])

        self.plnTxtEdt_note.setPlainText(self.ui_data["note"])
        self.cmbBx_frangeType.setCurrentIndex(self.ui_data["frange_type"]["index"])

        if self.ui_data_status["valid_list_selection"]:
            this_dag_data = self.dag_data[self.list_item_dag_path]
            cache_filename = this_dag_data["cache_filename"]
        else:
            cache_filename = self.ui_data["cache_filename"]
        self.lbl_cacheFilename.setText(cache_filename)

        if self.cmbBx_frangeType.currentText() == "Current Frame":
            self.LnEdt_elementFRange.setText(str(self.ui_data["current_frame"]))
        else:
            self.LnEdt_elementFRange.setText(str(self.ui_data["frange"]))

        # UPDATE UI STATE
        #     asset_type
        if self.assetType_enable():
            self.cmbBx_assetType.setEnabled(True)
        else:
            self.cmbBx_assetType.setEnabled(False)

        #     element_name
        if self.elementName_enable():
            self.LnEdt_elementName.setEnabled(True)
        else:
            self.LnEdt_elementName.setEnabled(False)

        #     filename display
        if self.filenameDisplay_enable():
            self.cmbBx_frangeType.setEnabled(True)
        else:
            self.lbl_cacheFilename.clear()
            self.cmbBx_frangeType.setEnabled(False)

        #     "Add ELEMENT to Cache-Out List"
        if self.addToCacheOutList_enable():
            self.btn_addCacheToList.setEnabled(True)
        else:
            self.btn_addCacheToList.setEnabled(False)

        #    frange_type
        if self.frangeType_enable():
            self.cmbBx_frangeType.setEnabled(True)
        else:
            self.cmbBx_frangeType.setEnabled(False)

        #    frange
        if self.frangeLineEdit_enable():
            self.LnEdt_elementFRange.setEnabled(True)
        else:
            self.LnEdt_elementFRange.setEnabled(False)

        #     "Cache Out"
        if self.cacheOut_enable():
            self.btn_cacheOut.setEnabled(True)
        else:
            self.btn_cacheOut.setEnabled(False)

    # --------------------------------------------------------------------
    def assetType_enable(self):
        return self.ui_data_status["valid_selected_transform"]

    def elementName_enable(self):
        return all([self.ui_data_status["valid_selected_transform"]])

    def filenameDisplay_enable(self):
        pred = self.ui_data_status["valid_selected_transform"] or self.ui_data_status["valid_list_selection"]
        return all([pred,
                    self.ui_data_status["valid_asset_type"],
                    self.ui_data_status["valid_element_name"],
                    ])

    def addToCacheOutList_enable(self):
        return all([self.ui_data_status["valid_selected_transform"],
                    self.ui_data_status["valid_asset_type"],
                    self.ui_data_status["valid_element_name"],
                    ])

    def update_dag_data_enable(self):
        return self.ui_data_status["valid_list_selection"]

    def editNotes_enable(self):
        return all([self.ui_data_status["valid_element_note"]])

    def frangeType_enable(self):
        pred1 = all([self.ui_data_status["valid_selected_transform"],
                     self.ui_data_status["valid_asset_type"],
                     self.ui_data_status["valid_element_name"],
                     ])
        pred2 = self.ui_data_status["valid_list_selection"]
        return pred1 or pred2

    def frangeLineEdit_enable(self):
        return self.ui_data_status["custom_frange_type"]

    def cacheOut_enable(self):
        return self.ui_data_status["valid_cache_out_list"]



class NoteEditor(QDialog):
    """NoteEditor

    """
    submit_note = Signal(str)

    def __init__(self, parent=None):
        LOG.debug(">>>>>>>>>>     NoteEditor.__init__")
        # Window Settings

        self.WINDOW_TITLE = "Please Enter Notes"
        self.WINDOW_VERSION = ""

        super(NoteEditor, self).__init__(parent)
        self.note = ""
        self.create_dialog()

    def create_dialog(self):
        """"""
        self.setWindowTitle("{0} {1}".format(self.WINDOW_TITLE,
                                             str(self.WINDOW_VERSION)
                                             ))

        def create_widgets():
            self.container = QWidget(self)
            self.plnTxtEdt_note = QPlainTextEdit(parent=self.container)
            self.btn_OK = QPushButton("OK", parent=self.container)

        def setup_widgets():
            self.plnTxtEdt_note.setReadOnly(False)
            self.plnTxtEdt_note.setEnabled(True)
            self.btn_OK.setEnabled(False)

        def layout_widgets():
            lyt_main = QVBoxLayout(self.container)
            lyt_note_widgets = QVBoxLayout(self.container)
            lyt_note_widgets.addWidget(self.plnTxtEdt_note)
            lyt_note_widgets.addWidget(self.btn_OK)
            lyt_main.addLayout(lyt_note_widgets)
            self.setLayout(lyt_main)

        def connect_signals():
            self.btn_OK.clicked.connect(self.cb_submit_note)
            self.plnTxtEdt_note.textChanged.connect(self.cb_validate_note)

        create_widgets()
        setup_widgets()
        layout_widgets()
        connect_signals()
        return self

    def cb_validate_note(self):
        """"""
        from re import search as re_search

        self.note = self.plnTxtEdt_note.toPlainText()
        if re_search("\w+", self.note):
            self.btn_OK.setEnabled(True)
        else:
            self.btn_OK.setEnabled(False)

    def cb_submit_note(self):
        self.submit_note.emit(self.note)
        self.plnTxtEdt_note.setPlainText("")
        self.close()
