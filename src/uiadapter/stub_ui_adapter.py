'''
Created on Jan 8, 2017

@author: kenmohamed
'''
from uiport import UIPresenterDispatcherBase


class UIController(object):
    '''
    The UIController 
        * Handles Multiple "buttons", each corresponding to a single Command
        * is the "Invoker/Waitress" of the Command-Pattern
    '''
    def __init__(self, ucside_uiport_cls=None):
        super(UIController, self).__init__()
        print self.__class__.__name__, " INIT"
        
        self._ucside_uiport_cls = ucside_uiport_cls
        
        self._usecase_invoker = None
        
        self._request_model_for_usecase = None

        
    '''
    The following methods would be called by ui-message-handlers that
    require access to the core -- IOW that invoke a use-case
    
    '''
    def set_request_model_for_usecase(self, data):
        self._request_model_for_usecase = data
        print "UIController.set_request_model_for_usecase, data: ", self._request_model_for_usecase
        
        
    def send_request_model(self):
        self._ucside_uiport_cls.request_model = self._request_model_for_usecase
        print "UIController.send_request_model, data: ", self._request_model_for_usecase
        
    def _pass_control_to_usecase(self):
        '''
        ui_button_pressed
        Called when the button is pressed.  Take the COMMAND currently 
        bound to the SLOT and call its execute() method
        
        slot_name string:usecase_name matching keys of self._command_slots
        use-case/slot-name is determined by the button pressed (or by some
        method/@Slot along the way to here)
        '''
        
        self.send_request_model()
        self._usecase_invoker.execute()
        

 
class UIPresenterDispatcher(UIPresenterDispatcherBase):
    '''
    Dispatches control to a Presenter based on data from self._request_model
    Assembler Instantiates this class and passes to the ui_adapter instance, 
    setting its _usecase_invoker instance variable
    '''
    presenter_dict = {}
    
    def __init__(self):
        super(UIPresenterDispatcher, self).__init__()
        print self.__class__.__name__, "INIT"
    
    def execute(self):
        presenter_name = self.response_model.keys()[0] + "Presenter"
        presenter_data = self.response_model.values()[0]
        
        print "\nPresenter Dispatcher - execute "
        print "Dispatcher - presenter_dict ", self.presenter_dict, type(self.presenter_dict)
        print "Dispatcher - presenter_name ", presenter_name, type(presenter_name)
        
        presenter_obj = self.presenter_dict[presenter_name]
        
        presenter_obj.set_response_data_for_presenter(presenter_data)
        presenter_obj.execute()
    
    def simulate(self):
        raise(NotImplementedError)
    


class PresenterBase(object):
     
    def __init__(self, uiside_uiport_cls=None):
        super(PresenterBase, self).__init__()
        print self.__class__.__name__, " PresenterBase INIT"
        
        self._uiside_uiport_cls = uiside_uiport_cls
        self._response_model = None
    
    def set_response_data_for_presenter(self, data):
        self._response_model = data
        
    def execute(self):
        self._run_presenter()
       
    
    '''
    def set_response_model(self, data):
        self._response_model = data
    
    def send_response_data(self):
        self._uiside_uiport_cls.response_model = self._response_model
     '''       
        
class UseCaseAPresenter(PresenterBase):
    '''
    The UseCaseA Presenter

    '''
    def __init__(self):
        super(UseCaseAPresenter, self).__init__()
        print self.__class__.__name__, " INIT"
        
    def _run_presenter(self):
        print "Present Data: ", self._response_model
        
        
        
class UseCaseBPresenter(PresenterBase):
    '''
    The UseCaseB Presenter

    '''
    def __init__(self):
        super(UseCaseBPresenter, self).__init__()
        print self.__class__.__name__, " INIT"
        
    def _run_presenter(self):
        print "Present Data: ", self._response_model
        
        
        
class UseCaseCPresenter(PresenterBase):
    '''
    The UseCaseC Presenter

    '''
    def __init__(self):
        super(UseCaseCPresenter, self).__init__()
        print self.__class__.__name__, " INIT"
        
      
    def _run_presenter(self):
        print "Present Data: ", self._response_model
        
# required by assembler
presenter_classes = [UseCaseAPresenter,
                     UseCaseBPresenter,
                     UseCaseCPresenter,
                     ]       
           
        