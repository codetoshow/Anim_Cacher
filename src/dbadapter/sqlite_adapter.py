# Python built-in modules.
import sys, os
import sqlite3
import inspect
import copy
# import sqlitebck
from datetime import datetime

import messenger


class SqliteAdapter(object):
    """ This class is a quick/simple API that handles SQLite database. """

    CLASS_NAME = inspect.stack()[0][3]

    def __init__(self, db_path=''):
        super(SqliteAdapter, self).__init__()

        self._messenger = messenger.Messenger()  # or displaying some output messages.
        self._db_path = db_path
        self._db_directory, self._dbName = os.path.split(self._db_path)
        self._db_handler = None
        self._connection_blocked = False
        self._is_connected = False

        #----------------------
        # Define table columns.
        #
        self._columns_item_tags = [
            ['it_id',             'text'],
            ['it_name',           'text'],
            ['it_category',       'text'],
            ['it_created',        'text'],
            ['it_created_site',   'text'],
            ['it_created_group',  'text'],
            ['it_created_user',   'text'],
            ['it_created_app',    'text'],
            ['it_pipe_status',    'text'],
            ['it_prod_status',    'text'],
            ['it_on_disk_state',  'text'],
            ['it_deletion_state', 'text'],
            ['it_modified',       'text'],
            ['it_modified_group', 'text'],
            ['it_modified_user',  'text'],
            
        ]
        self._columns_item_details = [
            ['it_id',                    'text'],
            ['it_description',           'text'],
            ['it_snapshot_dir',          'text'],
            ['it_output_dir',            'text'],
            ['it_output_name',           'text'],
            ['it_version',               'text'],
            ['it_frame_range_expr',      'text'],
            ['it_frame_paddings',        'text'],
            ['it_formats',               'text'],
            ['it_media_info_uuid',       'text'],
            ['it_upstream_dependency',   'text'],
            ['it_downstream_dependency', 'text'],
            ['it_file_info_uuid',        'text'],
            ['it_processing_log',        'text'],
            ['it_dag_path',              'text'],
            ['it_element_name',          'text'],
        ]

        self._cols_item_tags_name_to_value = {elemt[0]: '' for elemt in self._columns_item_tags}
        self._cols_item_tags_name_to_type = {elemt[0]: elemt[1] for elemt in self._columns_item_tags}
        self._cols_item_detail_name_to_value = {elemt[0]: '' for elemt in self._columns_item_details}
        self._cols_item_detail_name_to_type = {elemt[0]: elemt[1] for elemt in self._columns_item_details}
        self._cols_item_file_info = [
            'fi_id',
            'fi_amount',
            'fi_total_size',
            'fi_min_size',
            'fi_max_size'
        ]
        self._cols_item_media_info = [
            'md_id',
            'md_video_path',
            'md_thumbnail_path'
        ]

        #---------------------------------
        # Check tables while initializing.
        #
        self._eval_target_database()
        if self.connect():
            self._check_tables()
            self.disconnect()

    @property
    def item_tags_column_names(self):
        """ Method used to return ordered column names of item_tags table. """
        return [elemt[0] for elemt in self._columns_item_tags]

    @property
    def item_tags_column_data(self):
        """ Method used to return column data of item_tags table. """
        return copy.deepcopy(self._cols_item_tags_name_to_value)

    @property
    def item_detail_column_names(self):
        """ Method used to return ordered column names of item_detail table. """
        return [elemt[0] for elemt in self._columns_item_details]

    @property
    def item_detail_column_data(self):
        """ Method used to return column data of item_detail table. """
        return copy.deepcopy(self._cols_item_detail_name_to_value)

    def _eval_target_database(self):
        """ Private method used to evaluate target database. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if not os.path.exists(self._db_directory):
            try:
                os.makedirs(self._db_directory)
            except IOError as e:
                self._messenger.show('Failed to create %s' % self._db_directory)
                self._messenger.show('Database connection will be blocked.')
                self._connection_blocked = True
                return
        else:
            if os.path.exists(self._db_path):
                self._messenger.show('%s exists.' % self._db_path)
                return

        self._messenger.show('%s does not exist, will create one.' % self._db_path)

    def _check_tables(self):
        """ Private method used to check all tables. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        curs = self._db_handler.cursor()

        item_tags = curs.execute('SELECT name FROM sqlite_master WHERE type="table" AND name="item_tags"').fetchone()
        if not item_tags:
            data = [' '.join(elemt) for elemt in self._columns_item_tags]
            curs.execute('CREATE TABLE item_tags (%s)' % ','.join(data))
            self._messenger.show('item_tags table has been created.')
        else:
            self._messenger.show('item_tags table exists.')

        item_detail = curs.execute('SELECT name FROM sqlite_master WHERE type="table" AND name="item_detail"').fetchone()
        if not item_detail:
            data = [' '.join(elemt) for elemt in self._columns_item_details]
            curs.execute('CREATE TABLE item_detail (%s)' % ','.join(data))
            self._messenger.show('item_detail table has been created.')
        else:
            self._messenger.show('item_detail table exists.')

        # itemFileInfo = curs.execute('SELECT name FROM sqlite_master WHERE type="table" AND name="item_file_info"').fetchone()
        # if not itemFileInfo:
        #     curs.execute('CREATE TABLE item_file_info (%s)' % ','.join(self._cols_item_file_info))
        #     self._messenger.show('item_file_info table has been created.')
        # else:
        #     self._messenger.show('item_file_info table exists.')

        # itemMediaInfo = curs.execute('SELECT name FROM sqlite_master WHERE type="table" AND name="item_media_info"').fetchone()
        # if not itemMediaInfo:
        #     curs.execute('CREATE TABLE item_media_info (%s)' % ','.join(self._cols_item_media_info))
        #     self._messenger.show('item_media_info table has been created.')
        # else:
        #     self._messenger.show('item_media_info table exists.')

    def _build_meta_data(self, source_data={}, reference_data={}):
        """ Private method used to build meta data for later use. """

        for n, v in source_data.iteritems():
            if reference_data.has_key(n):
                reference_data[n] = v
        return reference_data

    def _has_valid_data(self, data={}):
        """ Private method used to check if there is valid value stored in the given data. """

        for val in data.values():
            if val:
                return True
        return False

    def _insert_item_data(self, source_data={}, column_data=[], target_table=''):
        """ Private method used to insert item data into table. """

        finalData = []
        for elemt in column_data:
            finalData.append(source_data[elemt[0]])

        curs = self._db_handler.cursor()
        curs.execute('INSERT INTO %s VALUES (%s)' % (target_table, ','.join(['?']*len(column_data))), finalData)
        curs.close()

    def connect(self):
        """ Method used to connect to database. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if not self._connection_blocked:
            self._db_handler = sqlite3.connect(self._db_path)
            self._messenger.show('Database connected.')
            self._is_connected = True
            return True
        else:
            self._messenger.show('Database connection was blocked.')
            self._is_connected = False
            return False

    def commit(self):
        """ Method used to commit changes to database. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))
        self._db_handler.commit()
        self._messenger.show('Changes have been committed to database.')

    def disconnect(self, commit=False):
        """ Method used to disconnect the database. This method can help committing changes before closing connection. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if self._db_handler:
            if commit:
                self._db_handler.commit()
            self._db_handler.close()
            self._messenger.show('Database dis_connected.')
            self._is_connected = False
        else:
            self._messenger.show('No database connection found.')

    def get_items(self, tags_filters={}):
        """ Method used to get row data from item_tags table. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        items = []

        if self._is_connected:
            curs = self._db_handler.cursor()
            keys = ' AND '.join([key+'=?' for key in tags_filters.keys()])
            filters = keys and 'WHERE %s' % keys or ''
            items = curs.execute('SELECT * FROM item_tags %s' % filters, (tags_filters.values())).fetchall()
            curs.close()
            self._messenger.show('Item data have been fetched. Found %s items.' % len(items))
        else:
            self._messenger.show('Dabaobei is not yet connected!', warning=True)

        return items

    def get_details_from(self, item_uuids=[], detail_filters={}):
        """ Method used to get detail data of multiple item by given item uuids. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if self._is_connected:
            curs = self._db_handler.cursor()
            keys = ' AND '.join([key+'=?' for key in detail_filters.keys()])
            filters = keys and 'AND %s' % keys or ''
            details = []
            for ituuid in item_uuids:
                detail = curs.execute('SELECT * FROM item_detail WHERE it_id=? %s' % filters, ([ituuid]+detail_filters.values())).fetchone()
                details.append(detail)
            curs.close()
            self._messenger.show('Item detail data have been fetched. Found %s details.' % len(details))
        else:
            self._messenger.show('Dabaobei is not yet connected!', warning=True)

        return details

    def get_versions_from(self, tags_filters={}, with_detail=False):
        """ Method used to quickly get available versions of a single item. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if self._is_connected:
            curs = self._db_handler.cursor()
            keys = ' AND '.join([key+'=?' for key in tags_filters.keys()])
            filters = keys and 'WHERE %s' % keys or ''
            items = curs.execute('SELECT * FROM item_tags %s' % filters, (tags_filters.values())).fetchall()

            # Find rest columns.
            restCols = self.item_detail_column_names
            restCols.remove('it_version')

            # Query data.
            versData = []
            for item in items:
                data = [curs.execute('SELECT it_version FROM item_detail WHERE it_id=?', (item[0],)).fetchone()]
                if with_detail:
                    data.append(curs.execute('SELECT %s FROM item_detail WHERE it_id=?' % ','.join(restCols), (item[0],)).fetchone())
                if data:
                    versData.append(data)

            # Build version data.
            if with_detail:
                versions = []
                for elemt in versData:
                    versions.append([elemt[0][0], {col: elemt[1][idx] for idx, col in enumerate(restCols)}])
            else:
                versions = [elemt[0][0] for elemt in versData]

            curs.close()
            self._messenger.show('Item version data have been fetched. Found %s versions.' % len(versions))
        else:
            versions = []
            self._messenger.show('Dabaobei is not yet connected!', warning=True)

        return versions

    def add_item(self, item_tags_data={}, item_detail_data={}):
        """ Method used to update tables with given table data. """

        self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))

        if self._is_connected:
            # Update item_tags table.
            if item_tags_data:
                # Prepare meta data for updating item_tags table.
                metaData = self._build_meta_data(item_tags_data, self._cols_item_tags_name_to_value.copy())
                # Insert item data into table.
                if self._has_valid_data(metaData):
                    self._insert_item_data(metaData, self._columns_item_tags, 'item_tags')
                    self._messenger.show('item_tags table has been updated.')

                    # Update item_detail table.
                    if item_detail_data:
                        # Prepare meta data for updating item_detail table.
                        metaData = self._build_meta_data(item_detail_data, self._cols_item_detail_name_to_value.copy())
                        # Insert item data into table.
                        if self._has_valid_data(metaData):
                            self._insert_item_data(metaData, self._columns_item_details, 'item_detail')
                            self._messenger.show('item_detail table has been updated.')
                        else:
                            self._messenger.show('No valid data contained in the given item detail data, skip updating item_detail table.', warning=True)
                    else:
                        self._messenger.show('item detail data is empty, skip updating item_detail table.')
                else:
                    self._messenger.show('No valid data contained in the given item tags data, skip updating item_tags table.', warning=True)
            else:
                self._messenger.show('item tags data is empty, skip updating item_tags table.')
        else:
            self._messenger.show('Dabaobei is not yet connected!', warning=True)
            
    def get_item_tags_column_data_type(self, name=''):
        """ Method used to return data type of the given column name. """

        return self._cols_item_tags_name_to_type.get(name)
    
    def get_item_detail_column_data_type(self, name=''):
        """ Method used to return data type of the given column name. """

        return self._cols_item_tags_name_to_type.get(name)

    # def backup(self, db_path=''):
    #     """ Method used to backup database. """
    #
    #     self._messenger.set_message_prefix('%s - %s' % (self.CLASS_NAME, inspect.stack()[0][3]))
    #
    #     if self.connect():
    #         if db_path:
    #             dstDB = sqlite3.connect(db_path)
    #         else:
    #             db_path = '%s/bck/baobei/baobei_%s.db' % (self._db_directory, datetime.strftime(datetime.now(), '%m%d%H%M%S%f'))
    #             try:
    #                 if not os.path.exists(os.path.dirname(db_path)):
    #                     os.makedirs(os.path.dirname(db_path))
    #                 dstDB = sqlite3.connect(db_path)
    #             except IOError as e:
    #                 dstDB = None
    #
    #         if dstDB:
    #             try:
    #                 sqlitebck.copy(self._db_handler, dstDB)
    #                 self._messenger.show('Database backup has been saved to %s' % db_path)
    #             except Exception, msg:
    #                 self._messenger.show('Unable to save out database backup to %s' % db_path, warning=True)
    #         else:
    #             self._messenger.show('No available db-connection for backup.', warning=True)
    #
    #         self.disconnect()
    #
    #         return db_path
    #
    #     return ''




if __name__ == "__main__":
    DB_PATH = "./test.db"
    db_instance = SqliteAdapter(DB_PATH)
