"""
Wrapper around DCC gui message commands
"""

# STANDARD IMPORTS
import inspect
import sys


class Messenger(object):
    """"""

    CLASS_NAME = inspect.stack()[0][3]

    def __init__(self, in_graphics_mode=False):
        super(Messenger, self).__init__()

        self._in_graphics_mode = in_graphics_mode
        self._dialog_title = "Messenger"
        self._prefix = "Messenger"
        self._gui_host = "std"
        self._board = self._std_board

    def set_in_graphics_mode(self, is_enabled=True):
        """"""
        self._in_graphics_mode = is_enabled

    def set_dialog_title(self, title="Messenger"):
        """"""
        self._dialog_title = title

    def set_message_prefix(self, prefix="Messenger"):
        """"""
        self._prefix = prefix

    def set_gui_host(self, host=""):
        """"""

        self._gui_host = host
        if host == "houdini":
            self._board = self._houdini_board
        elif host == "maya":
            self._board = self._maya_board
        elif host == "nuke":
            self._board = self._nuke_board
        elif host == "std" or host == "standard":
            self._board = self._std_board

    def _std_board(self, message="", warning=False, ask=False):
        """"""

        message = "[%s]%s%s\n" % (self._prefix, warning and "  W A R N I N G  | " or " ", message)
        sys.stdout.write(message)
        return 1

    def _maya_board(self, message="", warning=False, ask=False):
        """"""
        pass

    def _houdini_board(self, message="", warning=False, ask=False):
        """"""
        import hou

        return hou.ui.displayMessage(
                message,
                title=self._dialog_title,
                buttons=ask and ("No", "Yes") or ("OK",),
                severity=warning and hou.severityType.Warning or hou.severityType.Message
        )

    def _nuke_board(self, message="", warning=False, ask=False):
        """"""
        import nuke

        if ask:
            return nuke.ask(message)
        elif warning:
            message = "W A R N I N G\n\n%s" % message
            nuke.message(message)
        else:
            nuke.message(message)
        return None

    def show(self, message="", in_dialog=False, warning=False):
        """"""
        if in_dialog and self._gui_host != "std" and self._gui_host != "standard":
            return self._board(message, warning=warning)
        else:
            return self._std_board(message, warning=warning)

    def ask(self, message=""):
        """"""
        return self._board(message, ask=True)
