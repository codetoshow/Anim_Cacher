'''
Created on Sep 16, 2014

@author: kmohamed


import studio.utils.singleton as there_can_only_be_one

class Shot(there_can_only_be_one.Singleton):
    def __init__(self):
        super(Shot,self).__init__()


>>> a = Shot()
>>> b = Shot()
>>> print id(a)
79984288
>>> print id(b)
79984288

For thread safety, check out
http://www.garyrobinson.net/2004/03/python_singleto.html

'''


class Singleton(object):

    def __new__(cls, *list, **dict):
        if '_single_instance' not in cls.__dict__:
            cls._single_instance = super(
                Singleton, cls).__new__(cls, *list, **dict)
        return cls._single_instance


def _module_test():
    class One(Singleton):

        def __init__(self):
            print "class One instanced"

    class Two(Singleton):

        def __init__(self):
            print "class Two instanced"
        pass

    class Three(One):
        pass

    one = One()
    two = One()
    one1 = One()
    one2 = One()

    print id(one)
    print id(two)
    print ("---------------------")
    three = Two()

    print id(three)
    four = Three()
    print id(four)

if __name__ == '__main__':
    _module_test()
