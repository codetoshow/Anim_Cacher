"""
:created: Dec 19, 2015
:author: kenmohamed
"""
# STANDARD IMPORTS
from os import sys as os_sys
import subprocess

IS_WIN32 = "win32" in str(os_sys.platform).lower()


def subprocess_popen(*args, **kwargs):
    """ Wrapper to launch subprocess with a  *hidden* window """
    if IS_WIN32:
        startupinfo = subprocess.STARTUPINFO()
        flgs = subprocess.CREATE_NEW_CONSOLE | subprocess.STARTF_USESHOWWINDOW
        startupinfo.dwFlags = flgs
        startupinfo.wShowWindow = subprocess.SW_HIDE
        kwargs["startupinfo"] = startupinfo

    return subprocess.Popen(*args, **kwargs)

