

# STANDARD IMPORTS
from re import search as re_search
import os
import shutil
from tempfile import gettempdir as gettempdir

# THIRD PARTY IMPORTS
try:
    # Some functions will try to use this.. else it'll use python based
    # functions
    from PySide2.QtCore import QFile
    from PySide2.QtCore import QDir
    from PySide2.QtCore import QThreadPool
    from PySide2.QtCore import QRunnable
    from PySide2.QtCore import QCryptographicHash

except:
    print 'PySide2 could not be found...'
    pass



def create_local_space(folderName):
    """
    Creates a local folder for either a subprocess to work in, or a file to be
    saved to, etc.
    Set up to create folders under the running user's my documents folder
    @param folderName: mandatory argument. A list containing the folder
        structure (will be forced to a list)
    to be followed after the my documents structure.
    Ex: folderName=["playblasts", "S01_S0000"]

    140530 - edited to not use 'HOME'
    140723 - was messed up.  fixed it.<-- bad comment!
    """
    # myDocs = os.environ['HOME'].split("/")
    # force the required list type on the folderName param (safety first)
    if not isinstance(folderName, list):
        folderName = [folderName]

    temp_dir = gettempdir()
    localFolder = str(temp_dir) + "/" + str(folderName)

    if not os.path.isdir(localFolder):
        os.makedirs(localFolder)

    return localFolder


def create_local_copy(folderName, source, newName=None):
    """
    Creates a local folder for either a subprocess to work in, or a file to be
    saved to, etc.
    Set up to create folders/files under the running user's my documents folder
    Runs create_local_space
    @param folderName: mandatory argument. A list containing the folder
    structure to be followed after the my documents structure.
    Ex: folderName=["playblasts", "S01_S0000"]
    @param source: mandatory argument. The full path of the original file to
    be copied, as a string.
    @param newName:  optional argument.

    """
    localFolder = create_local_space(folderName)
    originalName = os.path.basename(source)
    if newName is not None:
        originalName = newName
    destination = localFolder + "/" + originalName
    copy_file(source, destination)
    return destination


def delete_folder(path):
    """
    remove the contents to a folder and then the folder itself
    @param path: the absolute path of the folder you wish to clear and delete
    """
    for fileName in os.listdir(path):
        filePath = "%s/%s" % (path, fileName)
        os.remove(filePath)
    os.rmdir(path)


def copy_file(src,
             dst,
             buffer_size=10485760,
             perserveFileDate=True,
             callback=None,
             force=False
             ):
    '''
    Faster copy_file (use in place of shutil.copy)

    @param src:    Source FILE
    @param dst:    Destination DIRECTORY
    @param buffer_size:    Buffer size to use during copy
    @param perserveFileDate:    Preserve the original file date

    Notes:
    Function from "http://blogs.blumetech.com/blumetechs-tech-blog/2011/05/faster-python-file-copy.html" 
    '''
    src_directory, src_filename = os.path.split(src)

    # If dst is a location without filename, use same filename as src
    if os.path.isdir(dst):
        dst = os.path.join(dst, src_filename)

    dst_directory, dst_filename = os.path.split(dst)
    # Check to make sure dst directory exists. If it doesn't create the
    # directory
    if(not(os.path.exists(dst_directory))):
        try:
            os.makedirs(dst_directory)
        except:
            pass
            #  WRAPPED in TRY: When this code is run in multiple threads,
            #    more than one thread can get past the "if" before any thread
            #    does the makedir
    try:
        # QFile.copy won't overwrite dst if dst.fildname==src.filename...
        #  the locations of the files is not considered
        if force:
            try:
                print "Removing: %s" % dst
                os.remove(dst)
            except:
                pass
        QFile.copy(src, dst)
    except:

        # something happened... try with basic "optimized" python
        #    Optimize the buffer for small files
        buffer_size = min(buffer_size, os.path.getsize(src))
        if(buffer_size == 0):
            buffer_size = 1024
        # ._samefile on Windows, only compares pathfilenames
        if shutil._samefile(src, dst):
            raise shutil.Error("`%s` and `%s` are the same file" % (src, dst))
        for fn in [src, dst]:
            try:
                st = os.stat(fn)
            except OSError:
                # File most likely does not exist
                pass
            else:
                # XXX What about other special files? (sockets, devices...)
                if shutil.stat.S_ISFIFO(st.st_mode):
                    raise shutil.SpecialFileError("`%s` is a named pipe" % fn)
        with open(src, 'rb') as fsrc:
            with open(dst, 'wb') as fdst:
                shutil.copyfileobj(fsrc, fdst, buffer_size)

    finally:
        pass

    if(perserveFileDate):
        shutil.copystat(src, dst)

    # if a callback was defined.. go ahead and run it.. src & dst passed
    if callback:
        callback(src, dst)


# Threading class for copies (If QRunnable exists)
try:
    class CopyFileThreadBase(QRunnable):

        def __init__(self, *args):
            super(CopyFileThreadBase, self).__init__(*args)
except:
    class CopyFileThreadBase(object):

        def __init__(self, *args):
            super(CopyFileThreadBase, self).__init__(*args)

        def run(self):
            return
    pass


class CopyFileThread(CopyFileThreadBase):

    def __init__(self,
                 src,
                 dst,
                 callback=None,
                 feedback=True,
                 force=False,
                 **kwargs
                 ):
        super(CopyFileThread, self).__init__(**kwargs)
        self._src = src
        self._dst = dst
        self._callback = callback
        self._result = None
        self._feedback = feedback
        self._force = force

    def _doCallback(self, src, dst, **kwargs):
        msg = ['FAIL', 'SUCCESS']
        if self._callback:
            self._callback(src, dst)
        if self._feedback:
            print 'copy [%s]: %s->%s' % (msg[self._result],
                                         self._src, self._dst)

    def run(self):
        copy_file(self._src, self._dst, force=self._force)

        if os.path.exists(self._dst):
            self._result = True
        else:
            self._result = False

        self._doCallback(self._src, self._dst)


def copy_files(srcDstList,
              useThreads=True,
              waitForDone=True,
              maxThreadCount=10,
              callback=None,
              force=False
              ):
    pool = None
    statusList = []
    frc = force
    # If possible, run the copy actions in threads
    if useThreads:
        try:
            pool = QThreadPool()
            pool.setMaxThreadCount(maxThreadCount)
        except:
            pass
    # Do the copy
    for srcDst in srcDstList:
        if pool:
            thread = CopyFileThread(srcDst[0], srcDst[1], force=frc)
            pool.start(thread)
        else:
            copy_file(srcDst[0], srcDst[1], callback=callback, force=frc)

    if pool and waitForDone:
        pool.waitForDone()

    # Build a "copy-success" list
    for srcDst in srcDstList:
        status = False
        if os.path.exists(srcDst[1]):
            status = True
        statusList.append(status)

    return statusList

##########


def compare_files(pathA, pathB):
    retVal = False

    fileA = QFile(pathA)
    fileB = QFile(pathB)

    # only bother if the files are the same size..
    if fileA.size() == fileB.size():
        hashA = QCryptographicHash(QCryptographicHash.Sha1)
        hashB = QCryptographicHash(QCryptographicHash.Sha1)
        hashA.addData(fileA.readAll())
        hashB.addData(fileB.readAll())
        if hashA.result() == hashB.result():
            retVal = True
    return retVal


# -----------------------------------------------------------------------------
#    Functions COPIED from tasks.py
#
def list_files(rootPath, ignorePattern=None, matchPattern=None, fullPath=True):
    '''Much faster way to list only Files. Uses QT libraries
    @param rootPath - string, the full rootpath of where to list dirs from
    @param ignorePattern - string, RE expression on what patterns
        to be skipped
    @param matchPattern - string, regular expression on what patterns are
                                to be included
    @return list - strings of full paths
    '''
    retVal = []
    dirs = QDir(rootPath)
    entryList = dirs.entryInfoList(QDir.Files)
    for entry in entryList:
        name = entry.absoluteFilePath().encode('ascii')
        basename = os.path.basename(name).encode('ascii')
        if basename and ignorePattern:
            if not re_search(ignorePattern, basename):
                if fullPath:
                    retVal.append(name)
                else:
                    retVal.append(basename)
        elif basename and matchPattern:
            if re_search(matchPattern, basename):
                if fullPath:
                    retVal.append(name)
                else:
                    retVal.append(basename)
        elif basename:
            if fullPath:
                retVal.append(name)
            else:
                retVal.append(basename)
    return retVal

# if __name__ == '__main__':
#    result = compare_files(r'W:\SPF\assets\chars\chr_ztest_ztest\data\looks\looksAnimDefault\textures\tex\normal\v2\chr_ztest_ztest_normal.1001.tex',
#                 r'W:\SPF\assets\chars\chr_ztest_ztest\data\looks\looksAnimDefault\textures\tex\normal\v3\chr_ztest_ztest_normal.1001.tex')
#    print result
