'''pySideUtils
:brief Adds some PyQt functionality to PySide

:author kmohamed
:date Added Dec. 17 2013
'''

# STANDARD
import xml.etree.ElementTree as xml
from cStringIO      import StringIO
import sys, os, re
import platform

# THIRD PARTY

from PySide2.QtWidgets import QWidget
from PySide2.QtCore import QObject
import PySide2.QtGui as QtGui

#from pysideuic      import compileUi

maya_executable_name = ""
current_platform = platform.system().lower()
if current_platform == 'nt':
    maya_executable_name = "maya\\.exe"
elif current_platform == 'darwin':
    maya_executable_name = "Maya"

if re.match(maya_executable_name, os.path.basename(sys.executable), re.I):
    # "Running in Maya."
    #import shiboken
    import maya.OpenMayaUI as omui


def loadUiType( ui_file ):
    """
    :brief Parse .ui File and return the form_class and base_class
    * form_class: has all of the form sub-widgets
    * base_class: from which to subclass the new widget.

    WHY?: PySide lacks the "loadUiType" command... so its functionality is
          duplicated here

    HOW?:
          * convert the ui file to py code (pyc) in a string buffer (aka "memory file")
          * execute it [in a new context ("frame") for a new namespace]
          * to retrieve the form_class and base_class.

     Originally from:Pablo Winant via Nathan Horne
     """
    parsed = xml.parse( ui_file )
    widget_class = parsed.find( 'widget' ).get( 'class' )
    form_class = parsed.find( 'class' ).text

    with open( ui_file, 'r' ) as f:
        # create a string buffer object
        o = StringIO()
        frame = {}

        compileUi( f, o, indent = 0 )
        pyc = compile( o.getvalue(), '<string>', 'exec' )
        exec pyc in frame

        # Fetch the base_class and form class based on their type in the
        # xml from designer
        form_class = frame['Ui_{0}'.format( form_class )]
        base_class = eval( 'QtGui.{0}'.format( widget_class ) )

    return base_class, form_class

def wrapinstance( ptr, base = None ):
    '''Wrapper for the shiboken.wrapInstance function.
    Convert a pointer to a Qt class instance.
    Returns the best possible matching class.

    sip.wrapinstance returns the best possible matching class while
    shiboken.wrapInstance does not.  This is a pain because you have to know
    the class of the object before you get it...
    :author Nathan Horne
    :param ptr: Pointer to QObject in memory
    :type ptr: long or Swig instance
    :param base: (Optional) Base class to wrap with (Defaults to QObject, which
    should handle anything)
    :type base: QWidget
    :return: QWidget or subclass instance
    :rtype: QWidget

    (kenm)140220 when I use this, via "maya_main_window" below, my windows "lose"the
    "Qt.WindowStaysOnTopHint"
    '''
    if ptr is None:
        return None

    ptr = long( ptr ) #Ensure type
    if globals().has_key( 'shiboken' ):
        if base is None:
            qObj = shiboken.wrapInstance(ptr, QObject )
            metaObj = qObj.metaObject()
            cls = metaObj.className()
            superCls = metaObj.superClass().className()
            if hasattr( QtGui, cls ):
                base = getattr( QtGui, cls )

            elif hasattr( QtGui, superCls ):
                base = getattr( QtGui, superCls )

            else:
                base = QWidget

        return shiboken.wrapInstance(ptr, base )
    else:
        return None


def maya_main_window():
    if re.match(maya_executable_name, os.path.basename(sys.executable), re.I):
        # "Running in Maya."
        main_window_ptr = omui.MQtUtil.mainWindow()
        return wrapinstance(long(main_window_ptr), QWidget)
    else:
        return None


