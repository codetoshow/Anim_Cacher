'''
Created on Dec 15, 2014

@author: kmohamed
'''
__updated__ = "2015-01-15"

# STANDARD IMPORTS
from nose.tools import assert_equal
from nose.tools import assert_not_equal

# THIRD PARTY IMPORTS

# NITROGEN IMPORTS
from studio.core.taskFramework.singleton import Singleton


def test_singleton_mod():
    class One(Singleton):

        def __init__(self):
            print "class One Instanced"

    class Two(Singleton):

        def __init__(self):
            print "class Two instanced"
        pass

    class Three(One):
        pass

    one = One()
    two = One()
    one1 = One()
    one2 = One()

    assert_equal(id(one), id(two))
    print id(one)
    print id(two)
    print ("---------------------")
    three = Two()

    print id(three)
    four = Three()
    print id(four)
    assert_not_equal(id(three), id(four))
