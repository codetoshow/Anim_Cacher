"""
data.py
Created on Dec 11, 2014

@author: kmohamed

@note: to provide a way to use this with multiple persistent data stores,
this data abstraction uses the singledispatch module.

singledispatch module:
a backport of functools.singledispatch from Python 3.4 back to Python 2.6-3.3

singledispatch provides a simple form of generic programming known
as single-dispatch generic functions...which lets you dispatch on ARGUMENT-TYPE,
more specifically, on the

        TYPE of the FIRST Argument

provided to the function

for more details, please see:
https://julien.danjou.info/blog/2013/python-3.4-single-dispatch-generic-function
"""

# STANDARD IMPORTS
import logging
from src.utils_.singledispatch import singledispatch as sd

#  LOGGER SETUP
#    create
loggers = list()
LOG = logging.getLogger(__name__)
loggers.append(LOG)
DATA_LOG = logging.getLogger("@Data")
loggers.append(DATA_LOG)


#   set level
for lggr in loggers:
    lggr.setLevel(logging.DEBUG)


LOG.debug("LOADED: %s", __name__)

__all__ = ["Data", "send_to", "get_from"]


class Data(object):
    """Key-Value store, supporting CRUD operations

    instance.data()               Return handle to ALL of the stored data
                                - for dev/debug and emergencies
    instance.data(key)           "getter"   READ
    instance.data(key, value)    "setter"   CREATE, UPDATE
    instance.datastore_delete(key)               DELETE

    @note: doesn't allow "value"=None as a temp value
    """

    def __init__(self, **kwargs):
        super(Data, self).__init__(**kwargs)
        DATA_LOG.debug("Data.__init__(): {}".format(self))

        self._data = dict()

    def data(self, *keyvalue):
        """provides CRUD operations on self.data"""
        if keyvalue:
            if len(keyvalue) == 2:  # if key and value:
                # ---------------------- Create, Update
                key = keyvalue[0]
                value = keyvalue[1]
                self._data[key] = value
                return None
            elif len(keyvalue) == 1:  # if key and not value:
                # ------------------------- Read
                key = keyvalue[0]
                try:
                    return self._data[key]
                except KeyError:
                    return None
            else:
                DATA_LOG.error("TOO MANY ARGUMENTS; accepts 0-2 arguments")

        # --------------------------------- retrieve
        else:  # if not key and not value:

            return self._data

    # ------------------------------------- Delete

    def delete(self, key):
        """deletes a key-value, given the key"""
        try:
            del self._data[key]
        except KeyError:
            DATA_LOG.warning("(DELETE); '%s' does not exist" % key)



@sd.singledispatch
def send_to(store_type, data):
    raise NotImplementedError("generic method with no default implementation")


@sd.singledispatch
def get_from(store_type):
    raise NotImplementedError("generic method with no default implementation")
