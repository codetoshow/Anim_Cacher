"""
job_framework.py

"job" is an atomic piece of work.  It can be processed locally or on a FARM

"""

# STANDARD IMPORTS
import logging
from os import path as os_path
import os
import shutil
import tempfile
import threading

# THIRD PARTY IMPORTS
import src.utils_.files as filesystem

# import studio.core.files as files

# import src.cacher.utils_.data as data
# import utils_.data as data
from data import Data

# LOGGERS
LOG_MOD = logging.getLogger(__name__)
LOG_CLS_JCR = logging.getLogger(__name__ + "@JobCodeResult")
LOG_CLS_J = logging.getLogger(__name__ + "@Job")
LOG_CLS_JQ = logging.getLogger(__name__ + "@JobQueue")
LOG_CLS_TWJ = logging.getLogger(__name__ + "@TempWorkspaceJob")
LOG_CLS_TWJQ = logging.getLogger(__name__ + "@TempWorkspaceJobQueue")

LOGGERS = list()
LOGGERS.append(LOG_MOD)
LOGGERS.append(LOG_CLS_JCR)
LOGGERS.append(LOG_CLS_J)
LOGGERS.append(LOG_CLS_JQ)
LOGGERS.append(LOG_CLS_TWJ)
LOGGERS.append(LOG_CLS_TWJQ)

#   S E T   L O G G I N G   L E V E L
for lggr in LOGGERS:
    lggr.setLevel(logging.INFO)
    lggr.setLevel(logging.DEBUG)

LOG_MOD.debug("LOADED: %s", __name__)


class Job(object):
    """
    Base-class for atomic job to run

    Job has three job-stages: pre, do, post

    Job-stages are realized here as the virtual methods:
        pre_job

        do_job

        post_job

    All jobs can be executed in THREADS if desired.

    Pass threading=True to Job.run() for Threaded execution.

    Callbacks can be added, to execute when any of the pre/do/post methods
    are completed.

    Callbacks live on a list and are run in the order they are added.

    self.code_result object is an instance of JobCodeResult().
    The intent is to use it to collect results/return-codes etc., from
    Job pre/do/post methods

    The object can then be queried by any callback function ( in the case
    where specific results require further action )

    """

    def __init__(self):
        super(Job, self).__init__()

        self.code_result = JobCodeResult()
        self._db = Data()
        self._db.data("callbacks", {})  # add in a callbacks dict

    def pre_job(self):
        """ PREPARE for the main job. """
        raise NotImplementedError

    def do_job(self):
        """ DO the main job """
        raise NotImplementedError

    def post_job(self):
        """ CLEANUP after the main job """
        raise NotImplementedError

    def add_callback(self, callback, job_stage=None):
        """
        Callbacks are run after job_stage methods complete.

        More than one callback can be defined. They are placed in a dict and
        called one after the other in the order added.

        @param callback - function to be called
        @param job_stage - string - "pre_job", "do_job", "post_job"
        """
        # get current callbacks dict
        callbacks = self._db.data("callbacks") or {}

        # add new callbacks
        if job_stage not in callbacks:
            callbacks[job_stage] = []
        callbacks[job_stage].append(callback)

        # save
        self._db.data("callbacks", callbacks)

    def get_callbacks(self, job_stage=None):
        """
        Returns the functions that are on the callback list.
        @return list - list of functions
        """
        callbacks = self._db.data("callbacks") or {}

        if job_stage in callbacks:
            return callbacks[job_stage]
        return []

    def run_callbacks(self, job_stage):
        """
        Run the defined callbacks for the give job_stage
        callbacks are passed the calling object as a parameter

        @param job_stage - string "pre_job", "do_job", "post_job
        """
        LOG_CLS_J.debug("run_callbacks: %s", job_stage)
        return_values = []
        callbacks = self.get_callbacks(job_stage)
        for callback in callbacks:
            if callback:
                return_values.append(callback(self))
        return return_values

    def run(self, threaded=False):
        """
        The execution / callback order for a job item.
        This function should not need to be redefined by future classes.

        A job runs:
            pre_job
            do_job
            post_job
            [callbacks] (defined via add_callback)
        @param threaded: Bool
            True: run all the methods in a separate thread
            False: run all the methods
        """
        if not threaded:  # run all the methods
            self.pre_job()
            self.run_callbacks("pre_job")
            self.do_job()
            self.run_callbacks("do_job")
            self.post_job()
            self.run_callbacks("post_job")
        else:
            the_thread = threading.Thread(target=self.run, args=([False]))
            the_thread.start()



class JobQueue(Job):
    """
    Defines a List of Jobs to be processed.

    This is an iterable class and jobs can be looped through via normal
    python functionality.

    do_job() - launches all jobs defined on the queue

    Iterators must provide two methods: __iter__()  and next()

     __iter__()  * returns the iterator object and is
                   IMPLICITLY called at the start of loops.
     next()      * returns the next value and is
                   IMPLICITLY called at each loop increment.
                 * raises a StopIteration exception when there are no
                   values to return. Exception is implicitly captured by
                   looping constructs to stop iterating.
    """

    def __init__(self):
        super(JobQueue, self).__init__()
        # iteration variables
        self._job_queue = []
        self._iterator_index = 0

    def pre_job(self):
        """ PREPARE for the main job. """
        pass

    def do_job(self):
        """
        DO the main job

        Run the queued jobs that have been added via add_item
        """
        for item in self:
            item.run()

    def post_job(self):
        """CLEANUP after the main job"""
        pass

    def add_item(self, job_object):
        """
        Add a job to the queue
        @param job_object - a Job-derived object
        """
        job_object._db.data("parent", self)
        self._job_queue.append(job_object)

    def add_items(self, job_objects):
        """ai"""
        for item in job_objects:
            self.add_item(item)

    # Iterator methods
    def __iter__(self):
        self._iterator_index = 0
        return self

    def next(self):
        """n"""
        try:
            result = self._job_queue[self._iterator_index]
        except IndexError:
            raise StopIteration
        self._iterator_index += 1
        return result

    def length(self):
        """l"""
        return len(self._job_queue)



class TempWorkspaceJob(Job):
    """
    Base-level class for Job processes that use a
    temporary workspace to perform work.

    USE CASE conditions:
        * data files on network need processing and
        * copying files would be a bottleneck
        * the processing across the network would be a speed bottleneck
    """

    def __init__(self):
        super(TempWorkspaceJob, self).__init__()

        # self.setTitle("Promote Item: Base Class")

        self.set_source_files(list)
        self.set_work_files(list)
        self.set_dest_files(list)

        self.set_final_files(list)

        self.set_dest_directory(None)
        self.set_work_directory(None)
        self.set_debug(False)

    def set_title(self, value):
        """st"""
        self._db.data("title", value)

    def get_title(self):
        """gt"""
        return self._db.data("title")

    def set_debug(self, enabled=False):
        """
        Set internal attribute to control debug features.

        @param enabled - bool
        """
        self._debug = enabled

    def get_debug(self):
        """
        Retrieve internal attribute to control debug features.

        @return bool
        """
        return self._debug

    def create_work_directory(self, root_temp_location=None):
        """
        Creates a work location.

        If not provided, this directory will be (only windows coded for now):
            os.environ["HOME"]/TempWorkspaceJob

        The work directory is removed when process is completed
        (unless DEBUG is enabled)

        Sets the set_work_directory upon creation

        @return string
        """
        if not root_temp_location:
            root_temp_location = "%s/TempWorkspaceJob" % os.environ["HOME"]

        if not os_path.exists(root_temp_location):
            os.makedirs(root_temp_location)

        # setup a subdirectory, for the "current" TempWorkspaceJob job
        temp_work_dir = tempfile.mkdtemp(dir=root_temp_location)
        self.set_work_directory(temp_work_dir)

        LOG_CLS_TWJQ.debug("CREATED WORK DIR: %s", self.get_work_directory())
        return temp_work_dir

    def set_work_directory(self, value):
        """
        Sets the working director variable for the class

        @param value - string, full path to a directory
        """
        self._db.data("work_dir", value)

        if os_path.exists(str(value)):
            os.chdir(value)
            LOG_CLS_TWJQ.debug("SET working directory: %s", value)

    def get_work_directory(self):
        """
        Retrieves the working directory variable for the class

        @return string
        """
        return self.get_value("work_dir")

    def set_dest_directory(self, value):
        """
        Sets the destination directory variable for the class
        This is where the files created in the promote process
        should be copied to.

        @param value - string to a full path
        """
        self.set_value("dest_dir", value)

    def get_dest_directory(self):
        """
        Retrieves the destination directory variable for the class

        @return string
        """
        return self.get_value("dest_dir")

    def add_source_file(self, filename):
        """
        Appends "filename" to an internal list to be processed.
        When pre_job begins
        the first file on this list is
            copied to the working directory and
            opened

        @param filename - string
        """
        src_list = self.get_source_files()
        src_list.append(filename)
        self.set_source_files(src_list)

    def set_source_files(self, file_list=list):
        """
        Sets the internal source file list in one go.
        See add_source_file for more details of behaviour

        @param file_list - list of full path for files
        """
        self._db.data("src_file_list", file_list)

    def get_source_files(self):
        """
        Retrieves the internal source file list

        @return list - list of strings
        """
        return self._db.data("src_file_list")

    def set_work_files(self, file_list=list):
        """
        Sets the internal working file list in one go.
        The workfiles are copies of the sourceFiles in the local work directory

        @param file_list - String List to file paths
        """
        self._db.data("wrk_file_list", file_list)

    def get_work_files(self):
        """
        Retrieves the internal list of working files
        @return - List of strings
        """
        return self._db.data("wrk_file_list")

    def set_dest_files(self, file_list=list):
        """
        Sets the internal list of files that need to be copied from the local
         working directory to the final destination directory

        @param file_list - List of paths
        """
        self._db.data("dst_file_list", file_list)

    def get_dest_files(self):
        """
        Retrieves the internal list of files that record what local files
        to copy to the final destination directory

        @return List of strings
        """
        return self._db.data("dst_file_list")

    def add_dest_file(self, filename):
        """
        Appends a file to the internal list of destination files
        (see set_dest_files)

        @param filename - string to file path
        """
        file_list = self.get_dest_files()
        file_list.append(filename)
        self.set_dest_files(file_list)

    def set_final_files(self, file_list=list):
        """
        Sets the internal list of files that HAVE BEEN copied from the
        local working directory to the final destination directory

        @param file_list - List of paths
        """
        self._db.data("final_file_list", file_list)

    def add_final_file(self, filename):
        """
        Appends a file to the internal list of the HAVE BEEN copied
        destination files (see set_final_files)

        @param filename - string to file path
        """
        file_list = self.get_final_files()
        file_list.append(filename)
        self.set_final_files(file_list)

    def get_final_files(self):
        """
        Retrieves the internal list of files of files that HAVE BEEN copied
        to the final destination directory

        @return List of strings
        """
        return self._db.data("final_file_list")

    # ------------------------------------------------------------------------
    def pre_job(self):
        """
        Implements baseclass virtual-method
        Sets up local working directory
        Copies the defined source files to local work directory
        Sets up the initial list of workFiles
        """

        LOG_CLS_TWJQ.info("=========================")
        LOG_CLS_TWJQ.info("pre_job...")

        # Setup a location from which to process files locally
        work_directory = self.create_work_directory()
        LOG_CLS_TWJQ.debug("pre_job work dir: %s", self.get_work_directory())

        # gather up some workspace variables
        file_list = self.get_source_files()

        # copy the required working files to local work_directory
        for item in file_list:
            try:
                copy_file(item, work_directory)
            except:
                LOG_CLS_TWJQ.error("Could not copy %s -> %s", item, work_directory)

        # record information for future processes
        work_file_list = os.listdir(work_directory)
        self.set_work_files(work_file_list)
        LOG_CLS_TWJQ.debug("workFileList: %s", work_file_list)

    def do_job(self):
        """
        Implements baseclass virtual-method

        The behaviour is to simply copy the workfiles defined to new
        non-versioned copies and record these new files to the
        destination file list.
        """

        # Gather up some work variables
        work_file_list = self.get_work_files()
        LOG_CLS_TWJQ.debug("workFileList: %s", work_file_list)

    def post_job(self):
        """
        Implements baseclass virtual-method

        Copies the list of destination files to the
        final output directory location.

        Unless set_debug is True the local working directory will be removed
        from system.
        """

        # Gather up some work variables
        work_folder = self.get_work_directory()
        dest_folder = self.get_dest_directory()
        dst_file_list = self.get_dest_files()

        LOG_CLS_TWJQ.info("====== post_job ========")
        LOG_CLS_TWJQ.info("work_folder:   %s", work_folder)
        LOG_CLS_TWJQ.info("dest_folder:   %s", dest_folder)
        LOG_CLS_TWJQ.info("dst_file_list: %s", dst_file_list)

        for dst_file in dst_file_list:
            src_pathfile = os_path.join(work_folder, dst_file)
            dst_pathfile = os_path.join(dest_folder, dst_file)
            if not os_path.exists(os_path.dirname(dst_pathfile)):
                LOG_CLS_TWJQ.debug("creating dir: %s", os_path.dirname(dst_pathfile))
                os.makedirs(os_path.dirname(dst_pathfile))

            # don"t over-write.. make it the next version
            if os_path.exists(dst_pathfile):
                dst_pathfile = jobs.getNextVersion(dst_pathfile)

            LOG_CLS_TWJQ.info("copy: %s -> %s", src_pathfile, dst_pathfile)
            copy_file(src_pathfile, dst_pathfile)
            self.add_final_file(dst_pathfile)

        # cleanup the temp work folder
        if not self.get_debug():
            if os.getcwd() == os_path.normpath(work_folder):
                # get out of the current directory (as the class sets it there)
                os.chdir("..")
                LOG_CLS_TWJQ.debug("cd ..")
            if not self.get_debug():
                LOG_CLS_TWJQ.debug("rmtree for temp work: %s", work_folder)
                shutil.rmtree(work_folder)
            else:
                LOG_CLS_TWJQ.debug("temp work kept: %s", work_folder)



class TempWorkspaceJobQueue(TempWorkspaceJob):
    """
    Defines a list of Jobs to be run.
    This is an iterable class and jobs can be looped through via normal
    python functionality.

    "do_job" - launches all jobs defined on the queue
    """

    def __init__(self):
        super(TempWorkspaceJobQueue, self).__init__()

        # iteration variables
        self._job_queue = []
        self._iterator_index = 0

    def add_item(self, job_object):
        """
        Add a job to the queue
        @param job_object - a Job-derived object
        """
        job_object._db.data("parent", self)
        self._job_queue.append(job_object)

    def add_items(self, job_objects):
        """s"""
        for item in job_objects:
            self.add_item(item)

    def do_job(self):
        """Run all the jobs that have been added via **add_item**"""
        super(TempWorkspaceJobQueue, self).do_job()
        for item in self:
            item.run()

    # Iterator methods
    def __iter__(self):
        self._iterator_index = 0
        return self

    def next(self):
        """n"""
        try:
            result = self._job_queue[self._iterator_index]
        except IndexError:
            raise StopIteration
        self._iterator_index += 1
        return result

    def length(self):
        """l"""
        return len(self._job_queue)



class JobCodeResult(object):
    """
    Base-class for code-related results ( return codes, errors).
    When a job finds an error it can create and store a
    JobCodeResult object.
    The intent is for callback functions to handle/use the
    data returned from the main processes (pre/do/post)
    """

    def __init__(self):
        super(JobCodeResult, self).__init__()

        self._result = Data()

    def set_result(self, value):
        """
        Stores information associated with the code result.
        Currently, this can be arbitrary information.

        @param value - string/object - some form of data
        """
        self._result.data("result", value)

    def get_result(self):
        """
        Retrieves the data associated with the **JobCodeResult**
        @return string/object - Whatever data was set with **set_result**
        """
        return self._result.data("result")



def copy_file(src,
              dst,
              buffer_size=10485760,
              preserve_file_date=True,
              callback=None,
              force=False
             ):
    """
    Faster copy_file (use in place of shutil.copy)

    @param src:    Source File
    @param dst:    Destination File (not file path)
    @param buffer_size:    Buffer size to use during copy
    @param preserve_file_date:    Preserve the original file date

    Notes:
    Function from "http://blogs.blumetech.com/blumetechs-tech-blog/2011/05/faster-python-file-copy.html"
    """
    _, src_file_name = os_path.split(src)  # "_" is unused src_parent

    # If dst is a location without filename, use same filename as src
    if os_path.isdir(dst):
        dst = os_path.join(dst, src_file_name)

    dst_parent, dst_file_name = os_path.split(dst)
    # Check to make sure dst directory exists. If it doesn't create the
    # directory
    if not os_path.exists(dst_parent):
        try:
            os.makedirs(dst_parent)
        except:
            pass
            #  WHY WRAPPED in TRY: When this code is run in multiple threads,
            #    more than one thread can get past the "if" before any thread
            #    does the makedir
    try:
        # QFile.copy won't overwrite dst if dst.fildname==src.filename...
        #  the locations of the files is not considered
        if force:
            try:
                print "Removing: %s", dst
                os.remove(dst)
            except:
                pass
        filesystem.QFile.copy(src, dst)
    except:

        # something happened... try with basic "optimized" python
        #    Optimize the buffer for small files
        buffer_size = min(buffer_size, os_path.getsize(src))
        if buffer_size == 0:
            buffer_size = 1024
        # ._samefile on Windows, only compares pathfilenames
        if shutil._samefile(src, dst):
            raise shutil.Error("`%s` and `%s` are the same file", (src, dst))
        for fn in [src, dst]:
            try:
                st = os.stat(fn)
            except OSError:
                # File most likely does not exist
                pass
            else:
                # XXX What about other special files? (sockets, devices...)
                if shutil.stat.S_ISFIFO(st.st_mode):
                    raise shutil.SpecialFileError("`%s` is a named pipe", fn)
        with open(src, "rb") as fsrc:
            with open(dst, "wb") as fdst:
                shutil.copyfileobj(fsrc, fdst, buffer_size)

    finally:
        pass

    if preserve_file_date:
        shutil.copystat(src, dst)

    # if a callback was defined.. go ahead and run it.. src & dst passed
    if callback:
        callback(src, dst)



def sample_callback(obj):
    """
    Illustrates use of JobCodeResult() to act on any results
    @param obj: reference to the calling object (Job() instance)
    """
    result = obj.code_result.get_result()
    # act on result
    if result:
        print result



def test():
    """useage test"""
    def _callback(obj):
        """callback method for test"""
        print "%s: hi", obj

    # Go through and setup a Queue of jobs
    queue = JobQueue()
    for i in range(0, 10):
        job = Job()
        job._db.data("title", "Jobs_%s", (10 - i))
        job._db.data("delay", i)
        job.add_callback(_callback)
        queue.add_item(job)
    # Run that queue
    queue.run()



if __name__ == "__main__":
    test()
