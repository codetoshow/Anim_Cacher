"""
Test the operation of the Data() utility class
"""

import pytest

# LOCAL IMPORTS
from src.utils_.data import Data


@pytest.fixture(scope="session")
def datastore():
    """ test setup
    instantiate Data object
    store test data
    return Data object
    """
    database = Data()
    #    save test data
    database.data("test_key", "test_value")
    return database


def test_data_store_retrieve_dict(datastore):
    full_dictionary = datastore.data()
    assert full_dictionary == {"test_key": "test_value"}


def test_data_store_read_value(datastore):
    stored_value = datastore.data("test_key")
    assert stored_value == "test_value"


def test_data_store_delete_value(datastore):
    stored_value = datastore.data("test_key")
    assert stored_value == "test_value"

    datastore.delete("test_key")

    stored_value = datastore.data("test_key")
    assert not stored_value
