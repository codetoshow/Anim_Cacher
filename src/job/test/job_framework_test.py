'''
job_framework_test.py

Created on Dec 15, 2014

@author: kmohamed


task_pre_to_post_test():
    Test the call stack "pre_task"->"do_task"->"post_task".
task_pre_to_post_THREADED_test():
    Test the call stack "pre_task"->"do_task"->"post_task" using Threads
task_callback_stack_test():
    Test the call stack "pre_task"->"do_task"->"post_task" with callbacks
task_callback_using_TaskCodeResult_test():
    Test the call stack "pre_task"->"do_task"->"post_task" with callbacks
    querying a TaskCodeResult object.

TODO
test    TaskQueue
        TempWorkspaceTask
        TempWorkspaceTaskQueue
'''
__updated__ = "2015-02-19"

# STANDARD IMPORTS
from nose.tools import assert_equal
from nose.tools import assert_not_equal
import threading
# THIRD PARTY IMPORTS

# NITROGEN IMPORTS
from studio.core.taskFramework.taskFramework import Task
from studio.core.taskFramework.taskFramework import TaskCodeResult
from studio.core.taskFramework.taskFramework import TaskQueue
from studio.core.taskFramework.taskFramework import TempWorkspaceTask
from studio.core.taskFramework.taskFramework import TempWorkspaceTaskQueue
# ----------------------------------------------------------------------------

# ____________________________________________________________________________
#
# Task() TESTS
# ____________________________________________________________________________


class Task0(Task):

    '''
    For use by task_pre_to_post_test()
    @attention: The run() method is not redefined in practice.  However,
    it is redefined here for testing purposes (to get return values for test..)

    '''

    def __init__(self):
        super(Task0, self).__init__()

    def pre_task(self):
        return 0

    def do_task(self):
        return 1

    def post_task(self):
        return 2

    def run(self, threaded=False):
        '''
        The execution / callback order for a task item.
        This function should not need to be redefined by future classes.

        A task runs:
            pre_task
            do_task
            post_task
            [callbacks] (defined via add_callback)
        @param threaded: Bool
            True: run all the methods in a separate thread
            False: run all the methods
        '''
        test_result = []
        if not threaded:  # run all the methods
            test_result.append(self.pre_task())
            self.run_callbacks('pre_task')
            test_result.append(self.do_task())
            self.run_callbacks('do_task')
            test_result.append(self.post_task())
            self.run_callbacks('post_task')
        else:
            the_thread = threading.Thread(target=self.run, args=(False))
            the_thread.start()
        return test_result


def task_pre_do_post_test():
    '''
    ### Task PRE-DO-POST STACK test
    Test the call stack "pre_task"->"do_task"->"post_task"


    '''
    atask = Task0()
    result = atask.run()

    expected_result = [0, 1, 2]
    assert_equal(result, expected_result)

# ----------------------------------------------------------------------------


class Task01(Task):

    '''
    For use by task_pre_do_post_THREADED_test()
    '''

    def __init__(self):
        super(Task01, self).__init__()

    def pre_task(self):
        return 0

    def do_task(self):
        return 1

    def post_task(self):
        return 2

    def run(self, threaded=True):
        '''
        The execution / callback order for a task item.
        This function should not need to be redefined by future classes.

        A task runs:
            pre_task
            do_task
            post_task
            [callbacks] (defined via add_callback)
        @param threaded: Bool
            True: run all the methods in a separate thread
            False: run all the methods
        '''
        def wrapper(func, args, result):
            result.append(func(*args))

        test_result = []
        if not threaded:  # run all the methods
            test_result.append(self.pre_task())
            self.run_callbacks('pre_task')
            test_result.append(self.do_task())
            self.run_callbacks('do_task')
            test_result.append(self.post_task())
            self.run_callbacks('post_task')
        else:
            result_from_thread = []
            the_thread = threading.Thread(target=wrapper,
                                          args=(self.run,
                                                [False],
                                                result_from_thread))
            the_thread.start()
            the_thread.join()
            test_result = result_from_thread
        return test_result


def task_pre_to_post_THREADED_test():
    '''
    ### Task PRE-DO-POST STACK test using THREADS
    Test the call stack "pre_task"->"do_task"->"post_task" using Threads

    # NOTE:
    "run" is extended here to return an overall result
    ... so this doesn't test "run" per-se
    '''
    atask = Task01()
    result = atask.run()

    expected_result = [[0, 1, 2]]
    assert_equal(result, expected_result)

# --------------------------------------------------------------------------


class Task1(Task):

    '''
    For use by task_callback_stack_test()
    '''

    def __init__(self):
        super(Task1, self).__init__()

    def pre_task(self):
        return 0

    def do_task(self):
        return 1

    def post_task(self):
        return 2

    def run(self, threaded=False):
        '''
        The execution / callback order for a task item.
        This function should not need to be redefined by future classes.

        A task runs:
            pre_task
            do_task
            post_task
            [callbacks] (defined via add_callback)
        @param threaded: Bool
            True: run all the methods in a separate thread
            False: run all the methods
        '''
        test_result = []
        if not threaded:  # run all the methods
            test_result.append(self.pre_task())
            test_result.append(self.run_callbacks('pre_task'))
            test_result.append(self.do_task())
            test_result.append(self.run_callbacks('do_task'))
            test_result.append(self.post_task())
            test_result.append(self.run_callbacks('post_task'))
        else:
            the_thread = threading.Thread(target=self.run, args=(False))
            the_thread.start()
        return test_result


def callback_func1(obj):
    return True


def task_callback_stack_test():
    '''
    ### Task PRE-DO-POST+CALLBACK STACK Test
    This test touches the following methods:
        * add_callback
        * get_callbacks
        * run_callbacks

    '''
    atask = Task1()
    callback_types = ['pre_task', 'do_task', 'post_task']

    # Add callbacks
    for callback_type in callback_types:
        atask.add_callback(callback_func1, callback_type)

    result = atask.run()
    expected_result = [0, [True], 1, [True], 2, [True]]
    assert_equal(result, expected_result)

# ----------------------------------------------------------------------------


class Task2(Task):

    '''
    For use by task_callback_using_TaskCodeResult_test()
    '''

    def __init__(self):
        super(Task2, self).__init__()

    def pre_task(self):
        # set a result for the callback to "handle"
        self.code_result.set_result("pre_task result")
        return 0

    def do_task(self):
        # set a result for the callback to "handle"
        self.code_result.set_result("do_task result")
        return 1

    def post_task(self):
        # set a result for the callback to "handle"
        self.code_result.set_result("post_task result")
        return 2

    def run(self, threaded=False):
        '''
        The execution / callback order for a task item.
        This function should not need to be redefined by future classes.

        A task runs:
            pre_task
            do_task
            post_task
            [callbacks] (defined via add_callback)
        @param threaded: Bool
            True: run all the methods in a separate thread
            False: run all the methods
        '''
        test_result = []
        if not threaded:  # run all the methods
            test_result.append(self.pre_task())
            test_result.append(self.run_callbacks('pre_task'))
            test_result.append(self.do_task())
            test_result.append(self.run_callbacks('do_task'))
            test_result.append(self.post_task())
            test_result.append(self.run_callbacks('post_task'))
        else:
            the_thread = threading.Thread(target=self.run, args=(False))
            the_thread.start()
        return test_result


def callback_func2(task_obj):
    result_code = task_obj.code_result.get_result()
    if result_code:
        return result_code

    return "boo"


def task_callback_using_TaskCodeResult_test():
    '''
    ### Task PRE-DO-POST+CALLBACK STACK with callbacks using TaskCodeResult Test
    This test touches the following methods:
        * add_callback
        * get_callbacks
        * run_callbacks

    '''
    atask = Task2()
    callback_types = ['pre_task', 'do_task', 'post_task']

    # Add callbacks
    for callback_type in callback_types:
        atask.add_callback(callback_func2, callback_type)

    result = atask.run()
    expected_result = [0,
                       ["pre_task result"],
                       1,
                       ["do_task result"],
                       2,
                       ["post_task result"]]
    assert_equal(result, expected_result)

#______________________________________________________________________________
#
#  TaskQueue TESTS
#______________________________________________________________________________


#==============================================================================
# def taskQueue_queue_test():
#     '''
# TaskQueue PRE-DO-POST Test
#
#     '''
#     task_queue = TaskQueue()
#     for i in range(4):
#         a_task = Task0()
#         task_queue.add_item(a_task)
#
#     results = task_queue.run()
#     assert_not_equal(results, None)
#==============================================================================
