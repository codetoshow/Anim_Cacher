'''

'''


# STANDARD IMPORTS
from nose.tools import assert_equal
from nose.tools import assert_not_equal

# THIRD PARTY IMPORTS


from studio.core.taskFramework.data import Data
from studio.core.taskFramework.data import Other
from studio.core.taskFramework.data import Other1
from studio.core.taskFramework.data import send_to
from studio.core.taskFramework.data import get_from


def test_memory_key_value_store():
    the_data = Data()

    data_key = 'test_key'
    data_value = 'test_value'

    data_to_store = {data_key: data_value}

    # store the data
    the_data.data(data_key, data_value)

    # retrieve the data
    data_stored = the_data.data()

    # assert
    assert_equal(data_stored, data_to_store)


def test_other_persistent_data_store():

    this_object = Other()

    data_in = {'test_data': 'test'}

    # write data to data store
    send_to(this_object, data_in)

    # retrieve data from data store
    data_out = get_from(this_object)

    assert_equal(data_out['test_data'], data_in['test_data'])


#______________________________________________________________________________
# def test_other1_persistent_data_store():
#
#     this_object = Other1()
#
#     data_in = {'test_data': 'test'}
#
# write data to data store
#     send_to(this_object, data_in)
#
# retrieve data from data store
#     data_out = get_from(this_object)
#
#     print "data_out: %s" % data_out
#     print "This test should fail"
#     assert_equal(data_out['test_data'], data_in['test_data'])
#
# test_other1_persistent_data_store.will_fail = True
#______________________________________________________________________________
